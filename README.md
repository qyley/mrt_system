# mrt_system

#### 介绍
MRT(Modulation Recognition Top)系统用于快速识别调制信号类型，纯verilog语言编写，可进行综合，搭载蜂鸟e203 MCU 软核以及协处理器。

#### 软件架构

1.  项目说明：https://www.jianguoyun.com/p/DWUDsKYQ4_n3CRjjq5cE 
2.  芯来Hummingbird e203 v2 mcu 使用手册：https://www.jianguoyun.com/p/DYF_HT8Q4_n3CRj7rJcE 


#### 安装教程

1.  先安装Vivado2018.3
2.  安装蜂鸟工具链NucleiStudio

#### 使用说明

1.  详见手册。联系qyley@foxmail.com
