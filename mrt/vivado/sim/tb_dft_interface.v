`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/11 20:32:02
// Design Name: 
// Module Name: tb_nice_mrt
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_dft_interface();

    // System	
    reg                         nice_clk             ;
    reg                         nice_rst_n	          ;
    wire                        nice_active	      ;
    wire                        nice_mem_holdup	  ;
//    wire                        nice_rsp_err_irq	  ;
    // Control cmd_req
    reg                         nice_req_valid       ;
    wire                        nice_req_ready       ;
    reg  [`E203_XLEN-1:0]       nice_req_inst        ;
    reg  [`E203_XLEN-1:0]       nice_req_rs1         ;
    reg  [`E203_XLEN-1:0]       nice_req_rs2         ;
    // Control cmd_rsp	
    wire                        nice_rsp_valid       ;
    reg                         nice_rsp_ready       ;
    wire [`E203_XLEN-1:0]       nice_rsp_rdat        ;
    wire                        nice_rsp_err    	  ;
    // Memory lsu_req	
    wire                        nice_icb_cmd_valid   ;
    reg                         nice_icb_cmd_ready   ;
    wire [`E203_ADDR_SIZE-1:0]  nice_icb_cmd_addr    ;
    wire                        nice_icb_cmd_read    ;
    wire [`E203_XLEN-1:0]       nice_icb_cmd_wdata   ;
//    wire [`E203_XLEN_MW-1:0]     nice_icb_cmd_wmask   ;  // 
    wire [1:0]                  nice_icb_cmd_size    ;
    // Memory lsu_rsp	
    reg                         nice_icb_rsp_valid   ;
    wire                        nice_icb_rsp_ready   ;
    reg  [`E203_XLEN-1:0]       nice_icb_rsp_rdata   ;
    reg                         nice_icb_rsp_err	;
    wire data_valid;
    wire [48-1:0] data;
    wire mrt_intr;
    wire data_last;
    
    reg t_nice_cmd_valid;
    reg [2:0] t_nice_cmd_short;
    reg [3:0] t_nice_rs1_short;
    reg [3:0] t_nice_rs2_short;
    wire [11:0] t_nice_core_cfg = {t_nice_cmd_valid,t_nice_cmd_short,t_nice_rs1_short,t_nice_rs2_short};
    
    wire [47:0] t_nice_core_res;
    wire [`E203_XLEN-1:0]       t_nice_rsp_rdat     ;
    wire                        t_nice_rsp_valid    ;
    wire [8-1:0]                t_nice_icb_addr_short;
    wire                        t_nice_icb_cmd_valid;    
    wire                        t_nice_icb_read     ;
    wire [4-1:0]                t_nice_icb_wdata_short;
    wire                        t_nice_mem_holdup    ;
    reg t_nice_core_isolate;
    assign t_nice_rsp_rdat = t_nice_core_res[47:16];
    assign t_nice_rsp_valid = t_nice_core_res[15];
    assign t_nice_icb_addr_short = t_nice_core_res[14:7];
    assign t_nice_icb_cmd_valid = t_nice_core_res[6];
    assign t_nice_icb_read = t_nice_core_res[5];
    assign t_nice_icb_wdata_short = t_nice_core_res[4:1];
    assign t_nice_mem_holdup = t_nice_core_res[0];

    
e203_subsys_nice_icb_bridge u_test(
    // System	
    .nice_clk         (nice_clk)    ,
    .nice_rst_n	        (nice_rst_n)  ,
    .nice_active	     (nice_active) ,
    .nice_mem_holdup	 (nice_mem_holdup)  ,
    // Control cmd_req
   .nice_req_valid      (nice_req_valid) ,
    . nice_req_ready     (nice_req_ready)  ,
   .nice_req_inst        (nice_req_inst),
    .nice_req_rs1      (nice_req_rs1)   ,
    .nice_req_rs2       (nice_req_rs2)  ,
    // Control cmd_rsp	
    .nice_rsp_valid      (nice_rsp_valid) ,
    .nice_rsp_ready     (nice_rsp_ready) ,
    .nice_rsp_rdat      (nice_rsp_rdat)  ,
    .nice_rsp_err    	 (nice_rsp_err) ,
    // Memory lsu_req	
    .nice_icb_cmd_valid  (nice_icb_cmd_valid) ,
   .nice_icb_cmd_ready   (nice_icb_cmd_ready),
    .nice_icb_cmd_addr   (nice_icb_cmd_addr),
    . nice_icb_cmd_read   (nice_icb_cmd_read) ,
    .nice_icb_cmd_wdata   (nice_icb_cmd_wdata),

    .nice_icb_cmd_size    (nice_icb_cmd_size),
    // Memory lsu_rsp	
    .nice_icb_rsp_valid   (nice_icb_rsp_valid),
    .nice_icb_rsp_ready  (nice_icb_rsp_ready) ,
    .nice_icb_rsp_rdata   (nice_icb_rsp_rdata),
    .nice_icb_rsp_err	(nice_icb_rsp_err),
    
    .u_mrt_wen           (data_valid),
    .u_mrt_cfg_data      (data),
    .u_mrt_cfg_last      (data_last),
    .u_mrt_finish        (mrt_intr),

    .t_nice_core_isolate (t_nice_core_isolate),
    .t_nice_core_cfg     (t_nice_core_cfg),
    .t_nice_core_res     (t_nice_core_res)

);

initial begin
    nice_clk     = 0        ;
    nice_rst_n	    = 1      ;
    nice_req_valid  = 0     ;
    nice_req_inst  = 32'b0      ;
    nice_req_rs1   = 32'b0      ;
    nice_req_rs2   = 32'b0      ;
    nice_rsp_ready    = 1   ;
    t_nice_cmd_valid  = 0;
    t_nice_cmd_short = 0;
    t_nice_rs1_short = 0;
    t_nice_rs2_short = 0;
    nice_icb_cmd_ready  = 1  ;
    t_nice_core_isolate = 1  ;
    nice_icb_rsp_valid = 0  ;
    nice_icb_rsp_rdata = 32'b0  ;
    nice_icb_rsp_err = 0	;
    
    #20 nice_rst_n <= 0;
    #20 nice_rst_n <= 1;
    
    #20 begin
        nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        //nice_req_inst <= 32'b0000010_00000_00000_100_00000_1111011; //start
        nice_req_valid <= 1;
        
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd0;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
        
    end
    #10 begin 
        nice_req_inst <= 32'b0;
        nice_req_valid <= 0;
        
        t_nice_cmd_valid = 1'b0;
        t_nice_cmd_short = 3'd0;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
    end
    
    #100 begin
        nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        //nice_req_inst <= 32'b0000010_00000_00000_100_00000_1111011; //start
        nice_req_valid <= 1;
        
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd2;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
        
    end
    #10 begin 
        nice_req_inst <= 32'b0;
        nice_req_valid <= 0;
        
        t_nice_cmd_valid = 1'b0;
        t_nice_cmd_short = 3'd0;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
    end
    #10
    repeat(2) @(posedge mrt_intr)begin
        #10 begin
        nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        //nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd1;
        nice_req_valid <= 1;
        
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd1;
        t_nice_rs1_short = 4'd1;
        t_nice_rs2_short = 4'd0;
        
        end
        #10 begin 
            nice_req_inst <= 32'b0;
            nice_req_valid <= 0;
            
            t_nice_cmd_valid = 1'b0;
            t_nice_cmd_short = 3'd0;
            t_nice_rs1_short = 4'd0;
            t_nice_rs2_short = 4'd0;
        end
        #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd2;
        nice_req_valid <= 1;
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd1;
        t_nice_rs1_short = 4'd2;
        t_nice_rs2_short = 4'd0;
        end
        #10 begin 
            nice_req_inst <= 32'b0;
            nice_req_valid <= 0;
            
            t_nice_cmd_valid = 1'b0;
            t_nice_cmd_short = 3'd0;
            t_nice_rs1_short = 4'd0;
            t_nice_rs2_short = 4'd0;
        end
        #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd3;
        nice_req_valid <= 1;
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd1;
        t_nice_rs1_short = 4'd3;
        t_nice_rs2_short = 4'd0;
        end
        #10 begin 
            nice_req_inst <= 32'b0;
            nice_req_valid <= 0;
            
            t_nice_cmd_valid = 1'b0;
            t_nice_cmd_short = 3'd0;
            t_nice_rs1_short = 4'd0;
            t_nice_rs2_short = 4'd0;
        end
        #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd4;
        nice_req_valid <= 1;
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd1;
        t_nice_rs1_short = 4'd4;
        t_nice_rs2_short = 4'd0;
        end
        #10 begin 
            nice_req_inst <= 32'b0;
            nice_req_valid <= 0;
            t_nice_cmd_valid = 1'b0;
            t_nice_cmd_short = 3'd0;
            t_nice_rs1_short = 4'd0;
            t_nice_rs2_short = 4'd0;
        end
    end
    
    #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000010_00000_00000_100_00000_1111011; //start
        nice_req_valid <= 1;
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd2;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
    end
    #10 begin 
        nice_req_inst <= 32'b0;
        nice_req_valid <= 0;
        t_nice_cmd_valid = 1'b0;
        t_nice_cmd_short = 3'd0;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
    end
    
    #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat   pro mode
        nice_req_rs1 <= 32'h8000_0020;
        nice_req_valid <= 1;
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd1;
        t_nice_rs1_short = 4'b1100;
        t_nice_rs2_short = 4'd0;
    end
    #10 begin 
        nice_req_inst <= 32'b0;
        nice_req_valid <= 0;
        t_nice_cmd_valid = 1'b0;
        t_nice_cmd_short = 3'd0;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
    end
    #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd0;
        nice_req_valid <= 1;
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd1;
        t_nice_rs1_short = 4'b0000;
        t_nice_rs2_short = 4'd0;
        end
    #10 begin 
        nice_req_inst <= 32'b0;
        nice_req_valid <= 0;
        t_nice_cmd_valid = 1'b0;
        t_nice_cmd_short = 3'd0;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
    end
    
    #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000010_00000_00000_100_00000_1111011; //start
        nice_req_valid <= 1;
        t_nice_cmd_valid = 1'b1;
        t_nice_cmd_short = 3'd2;
        t_nice_rs1_short = 4'd0;
        t_nice_rs2_short = 4'd0;
    end
    #10 begin 
        t_nice_core_isolate <= 0;
        nice_req_inst <= 32'b0;
        nice_req_valid <= 0;
    end
    
    repeat(2) @(posedge mrt_intr)begin
        #10 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd1;
        nice_req_valid <= 1;
        end
        #10 begin 
            nice_req_inst <= 32'b0;
            nice_req_valid <= 0;
        end
        #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd2;
        nice_req_valid <= 1;
        end
        #10 begin 
            nice_req_inst <= 32'b0;
            nice_req_valid <= 0;
        end
        #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd3;
        nice_req_valid <= 1;
        end
        #10 begin 
            nice_req_inst <= 32'b0;
            nice_req_valid <= 0;
        end
        #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd4;
        nice_req_valid <= 1;
        end
        #10 begin 
            nice_req_inst <= 32'b0;
            nice_req_valid <= 0;
        end
    end
    #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000010_00000_00000_100_00000_1111011; //start
        nice_req_valid <= 1;
    end
    #10 begin 
        nice_req_inst <= 32'b0;
        nice_req_valid <= 0;
    end
    #100 begin
        //nice_req_inst <= 32'b0000100_00000_00000_111_00000_1111011;//ldbuf
        nice_req_inst <= 32'b0000001_00000_00000_110_00000_1111011; //rdstat
        nice_req_rs1 <= 32'd0;
        nice_req_valid <= 1;
        end
    #10 begin 
        nice_req_inst <= 32'b0;
        nice_req_valid <= 0;
    end
    #100 $finish;
end
    always #5 nice_clk <= ~nice_clk;
    
   ADC_enframe_block_sim u_adc_sim(
    .i_clk(nice_clk),
    .i_rstn(nice_rst_n),
    .i_ena(1'b1),
    .o_data(data),
    .o_data_valid(data_valid),
    .o_data_last(data_last),
    .signal_type(4'b1)
    );
endmodule
