`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/11 14:20:49
// Design Name: 
// Module Name: tb_mrt_wrp
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_mrt_wrp#(
    parameter T = 10
)();
    
    reg clk;
    reg rstn;
    wire [31:0] u42a_data, sigma_dp_data;
    wire u42a_data_valid, sigma_dp_data_valid;
    wire data_valid;
    wire [48-1:0] data;
    wire data_last;
        
    MRT_wrapper u_test(
   .i_clk                  (clk) ,
   .i_rstn                 (rstn),
   .i_ena                  (1'b1),
   .i_wen                  (data_valid),
   .i_cfg_data             (data),
   .i_cfg_last             (data_last),
   .u42a_data              (u42a_data), //29bit fra 
   .u42a_data_valid        (u42a_data_valid),
   .sigma_dp_data          (sigma_dp_data), //25bit fra
   .sigma_dp_data_valid    (sigma_dp_data_valid)
    );
    
   initial begin
            clk = 0;
            rstn = 0;
            #(2*T) rstn = 1;
   end
        
   always #(T/2) clk <= ~clk;

    localparam       nums= 8192; 
    reg [24-1:0]      db1 [nums-1:0];
    initial $readmemh("D:/table/Project/Hbirdv2_ego/data/III24bit.txt",db1);
    reg [24-1:0]      db2 [nums-1:0];
    initial $readmemh("D:/table/Project/Hbirdv2_ego/data/QQQ24bit.txt",db2);
    integer cyc_cnt = 0;
    reg [13-1:0] db_addr = 0;
    always @(posedge clk) begin
        cyc_cnt = cyc_cnt + 1;
        db_addr <= db_addr + 1;
        // 1.2M sample rate versus 16M clock, each frame(8192 samples) takes about 109226 clock
        if(cyc_cnt == 109226/5)begin
            db_addr <= 0;
            cyc_cnt <= 0;
        end
    end
    
    assign data_valid = cyc_cnt < 8192;
    assign data = {db2[db_addr],db1[db_addr]};
    assign data_last = cyc_cnt==8191;
endmodule
