//****************************************Copyright (c)***********************************//
//原子哥在线教学平台：www.yuanzige.com
//技术支持：www.openedv.com
//淘宝店铺：http://openedv.taobao.com 
//关注微信公众平台微信号："正点原子"，免费获取ZYNQ & FPGA & STM32 & LINUX资料。
//版权所有，盗版必究。
//Copyright(C) 正点原子 2018-2028
//All rights reserved                                  
//----------------------------------------------------------------------------------------
// File name:           eth_arp_test
// Last modified Date:  2020/2/13 9:20:14
// Last Version:        V1.0
// Descriptions:        以太网ARP测试实验
//----------------------------------------------------------------------------------------
// Created by:          正点原子
// Created date:        2020/2/13 9:20:14
// Version:             V1.0
// Descriptions:        The original version
//
//----------------------------------------------------------------------------------------
//****************************************************************************************//

module eth_arp_test(
    input              sys_clk_n   , //系统时钟
    input              sys_clk_p   , //系统时钟
    input              sys_rst     , //系统复位信号，常低 
    input              touch_key   , //触摸按键,用于触发开发板发出ARP请求
    //PL以太网SGMII接口   
    input              ref_clk_n   ,
    input              ref_clk_p   ,
    output             eth_rst_n   ,
    input              eth_rx_n    ,
    input              eth_rx_p    ,
    output             eth_tx_n    ,
    output             eth_tx_p    ,
    //MDIO接口
    output             eth_mdc     , //PHY管理接口的时钟信号
    inout              eth_mdio      //PHY管理接口的双向数据信号
    );

//parameter define
//开发板MAC地址 00-11-22-33-44-55
parameter  BOARD_MAC = 48'h00_11_22_33_44_55;     
//开发板IP地址 192.168.1.10     
parameter  BOARD_IP  = {8'd192,8'd168,8'd1,8'd10};
//目的MAC地址 ff_ff_ff_ff_ff_ff
parameter  DES_MAC   = 48'hff_ff_ff_ff_ff_ff;
//目的IP地址 192.168.1.102
parameter  DES_IP    = {8'd192,8'd168,8'd1,8'd102};
//输入数据IO延时(如果为n,表示延时n*78ps) 
parameter IDELAY_VALUE = 16;

//wire define
wire          clk_200m   ; //用于IO延时的时钟 
              
wire          gmii_rx_clk; //GMII接收时钟
wire          gmii_rx_dv ; //GMII接收数据有效信号
wire  [7:0]   gmii_rxd   ; //GMII接收数据
wire          gmii_tx_clk; //GMII发送时钟
wire          gmii_tx_en ; //GMII发送数据使能信号
wire  [7:0]   gmii_txd   ; //GMII发送数据     

wire          arp_gmii_tx_en; //ARP GMII输出数据有效信号 
wire  [7:0]   arp_gmii_txd  ; //ARP GMII输出数据
wire          arp_rx_done   ; //ARP接收完成信号
wire          arp_rx_type   ; //ARP接收类型 0:请求  1:应答
wire  [47:0]  src_mac       ; //接收到目的MAC地址
wire  [31:0]  src_ip        ; //接收到目的IP地址    
wire          arp_tx_en     ; //ARP发送使能信号
wire          arp_tx_type   ; //ARP发送类型 0:请求  1:应答
wire  [47:0]  des_mac       ; //发送的目标MAC地址
wire  [31:0]  des_ip        ; //发送的目标IP地址   
wire          arp_tx_done   ; //ARP发送完成信号

wire          udp_gmii_tx_en; //UDP GMII输出数据有效信号 
wire  [7:0]   udp_gmii_txd  ; //UDP GMII输出数据
wire          rec_pkt_done  ; //UDP单包数据接收完成信号
wire          rec_en        ; //UDP接收的数据使能信号
wire  [31:0]  rec_data      ; //UDP接收的数据
wire  [15:0]  rec_byte_num  ; //UDP接收的有效字节数 单位:byte 
wire  [15:0]  tx_byte_num   ; //UDP发送的有效字节数 单位:byte 
wire          udp_tx_done   ; //UDP发送完成信号
wire          tx_req        ; //UDP读数据请求信号
wire  [31:0]  tx_data       ; //UDP待发送数据
wire          tx_start_en   ;

wire sys_rst_n;
wire gmii_clk;
wire locked;
wire [15:0] sv16;

//*****************************************************
//**                    main code
//*****************************************************


mdio_config u_mdio_cfg(
    .sys_clk_200m  (clk_200m),
    .sys_rst(sys_rst),
    //MDIO接口
    .eth_mdc (eth_mdc) , //PHY管理接口的时钟信号
    .eth_mdio (eth_mdio), //PHY管理接口的双向数据信号
    .eth_rst_n (), //以太网复位信号
    
    .touch_key (touch_key) //触摸按键
);


assign tx_start_en = rec_pkt_done;
assign tx_byte_num = 16'd4;
assign des_mac = src_mac;
assign des_ip = src_ip;
assign eth_rst_n = sys_rst_n;

assign des_mac = src_mac;
assign des_ip = src_ip;
assign eth_rst_n = sys_rst_n;
assign sys_rst_n = ~sys_rst;
assign gmii_rx_clk = gmii_clk;
assign gmii_tx_clk = gmii_clk;


IBUFDS uclk200 (
    .O (clk_200m),
    .I (sys_clk_p),
    .IB (sys_clk_n));

//SGMII接口 -- GMII接口 bridge
gig_ethernet_pcs_pma_0 u_gmii_to_sgmii(
    //gmii Interace
    .gmii_rx_dv(gmii_rx_dv), //output
    .gmii_rx_er(), //output
    .gmii_rxd(gmii_rxd),   //output [7:0]
    .gmii_tx_en(gmii_tx_en),   //input
    .gmii_tx_er(1'b0),   //input
    .gmii_txd(gmii_txd),     //input [7:0]
    //ref clk
    .gtrefclk_n(ref_clk_n),
    .gtrefclk_p(ref_clk_p),
    //sys input
    .independent_clock_bufg(clk_200m),
    .configuration_vector(5'b00000), //4:0
    .speed_is_10_100(1'b0),
    .speed_is_100(1'b0),
    .reset(sys_rst),
    .signal_detect(1'b1),
    //sgmii interface
    .rxn(eth_rx_n),
    .rxp(eth_rx_p),
    .txn(eth_tx_n),
    .txp(eth_tx_p),
    //sys output omitted
    .gtrefclk_bufg_out(gmii_clk),
    .status_vector(sv16)
);

//ARP通信
arp                                             
   #(
    .BOARD_MAC     (BOARD_MAC),      //参数例化
    .BOARD_IP      (BOARD_IP ),
    .DES_MAC       (DES_MAC  ),
    .DES_IP        (DES_IP   )
    )
   u_arp(
    .rst_n         (sys_rst_n  ),
                    
    .gmii_rx_clk   (gmii_rx_clk),
    .gmii_rx_dv    (gmii_rx_dv ),
    .gmii_rxd      (gmii_rxd   ),
    .gmii_tx_clk   (gmii_tx_clk),
    .gmii_tx_en    (arp_gmii_tx_en ),
    .gmii_txd      (arp_gmii_txd),
                    
    .arp_rx_done   (arp_rx_done),
    .arp_rx_type   (arp_rx_type),
    .src_mac       (src_mac    ),
    .src_ip        (src_ip     ),
    .arp_tx_en     (arp_tx_en  ),
    .arp_tx_type   (arp_tx_type),
    .des_mac       (des_mac    ),
    .des_ip        (des_ip     ),
    .tx_done       (arp_tx_done)
    );

//UDP通信
udp                                             
   #(
    .BOARD_MAC     (BOARD_MAC),      //参数例化
    .BOARD_IP      (BOARD_IP ),
    .DES_MAC       (DES_MAC  ),
    .DES_IP        (DES_IP   )
    )
   u_udp(
    .rst_n         (sys_rst_n   ),  
    
    .gmii_rx_clk   (gmii_rx_clk ),           
    .gmii_rx_dv    (gmii_rx_dv  ),         
    .gmii_rxd      (gmii_rxd    ),                   
    .gmii_tx_clk   (gmii_tx_clk ), 
    .gmii_tx_en    (udp_gmii_tx_en),         
    .gmii_txd      (udp_gmii_txd),  

    .rec_pkt_done  (rec_pkt_done),    
    .rec_en        (rec_en      ),     
    .rec_data      (rec_data    ),         
    .rec_byte_num  (rec_byte_num),      
    .tx_start_en   (tx_start_en ),        
    .tx_data       (tx_data     ),         
    .tx_byte_num   (tx_byte_num ),  
    .des_mac       (des_mac     ),
    .des_ip        (des_ip      ),    
    .tx_done       (udp_tx_done ),        
    .tx_req        (tx_req      )           
    ); 

//以太网控制模块
eth_ctrl u_eth_ctrl(
    .clk            (gmii_rx_clk),
    .rst_n          (sys_rst_n),

    .arp_rx_done    (arp_rx_done   ),
    .arp_rx_type    (arp_rx_type   ),
    .arp_tx_en      (arp_tx_en     ),
    .arp_tx_type    (arp_tx_type   ),
    .arp_tx_done    (arp_tx_done   ),
    .arp_gmii_tx_en (arp_gmii_tx_en),
    .arp_gmii_txd   (arp_gmii_txd  ),
                     
    .udp_gmii_tx_en (udp_gmii_tx_en),
    .udp_gmii_txd   (udp_gmii_txd  ),
                     
    .gmii_tx_en     (gmii_tx_en    ),
    .gmii_txd       (gmii_txd      )
    );

wire mcu_clk;
wire wr_rst_busy,rd_rst_busy;
wire fifo_full,fifo_empty;
wire [63:0] dout;
wire rd_en;
wire wr_en;

clk_wiz_1 u_mcu_clk(
    .clk_in1(clk_200m),
    .clk_out1(mcu_clk),
    .resetn(sys_rst_n)
);

asyn_udp_rx_fifo u_asyn_fifo(
    .wr_clk(gmii_rx_clk),
    .rd_clk(mcu_clk),
    .rst(sys_rst),
    .wr_rst_busy(wr_rst_busy),
    .rd_rst_busy(rd_rst_busy),
    .full(fifo_full),
    .empty(fifo_empty),
    .din(rec_data),
    .dout(dout),
    .wr_en(wr_en),
    .rd_en(rd_en)
);

reg dout_vld;
assign rd_en = ~fifo_empty & ~rd_rst_busy;
assign wr_en = ~fifo_full & rec_en & ~wr_rst_busy;
always@(posedge mcu_clk)begin
    if(~sys_rst_n) 
        dout_vld <= 1'b0;
    else
        dout_vld <= rd_en;
end

//wire ram0_i_wen, ram0_q_wen;
//wire [12:0] ram0_i_addr,ram0_q_addr;
//wire [23:0] adc_data_i0, adc_data_q0;
//wire ram0_wen;
reg [13:0] waddr;              // 0~8192*2-1 w=14
//assign ram0_i_wen = rec_en & ~waddr[13];
//assign ram0_q_wen = rec_en & waddr[13];
//assign ram0_wen = rec_en;
//assign ram0_i_addr = ram0_i_wen  ? waddr[12:0] : 13'b0;
//assign ram0_q_addr = ram0_q_wen  ? waddr[12:0] : 13'b0;
//assign tx_data = waddr;

//adc_i_ram u_adc_i_ram0(
//    .clk(gmii_rx_clk), 
//    .din(rec_data[23:0]), 
//    .addr(ram0_i_addr),
//    .cs(1'b1),
//    .we(ram0_i_wen),
//    .wem({3{1'b1}}),
//    .dout(adc_data_i0)
//    );
    
//    adc_q_ram u_adc_q_ram0(
//    .clk(gmii_rx_clk), 
//    .din(rec_data[23:0]), 
//    .addr(ram0_q_addr),
//    .cs(1'b1),
//    .we(ram0_q_wen),
//    .wem({3{1'b1}}),
//    .dout(adc_data_q0)
//    );

    always @(posedge gmii_rx_clk)begin
        if(~sys_rst_n) waddr <= 14'd0;
        else waddr <=  (rec_en&~wr_rst_busy)  ? waddr+1 : waddr;
    end


ila_0 u_ila(
    .clk(gmii_clk),
    .probe0(mcu_clk),
    .probe1(dout[32+8-1:32]),
    .probe2(fifo_full),
    .probe3(dout[8-1:0]),
    .probe4(fifo_empty),
    .probe5(dout_vld),
    .probe6(rec_en),
    .probe7(rec_data),
    .probe8(waddr),
    .probe9(wr_en),
    .probe10(u_udp.u_udp_rx.ip_pkg_byte_num)
    );

endmodule


module adc_i_ram 
#(parameter DP = 8192,
  parameter FORCE_X2ZERO = 0,
  parameter DW = 24,
  parameter MW = 3,
  parameter AW = 13 
)
(
  input             clk, 
  input  [DW-1  :0] din, 
  input  [AW-1  :0] addr,
  input             cs,
  input             we,
  input  [MW-1:0]   wem,
  output [DW-1:0]   dout
);

    reg [DW-1:0] mem_r [0:DP-1];
    
    reg [AW-1:0] addr_r;
    wire [MW-1:0] wen;
    wire ren;

    assign ren = cs & (~we);
    assign wen = ({MW{cs & we}} & wem);


    always @(posedge clk)
    begin
        if (ren) begin
            addr_r <= addr;
        end
    end
    genvar i;
    generate
      for (i = 0; i < MW; i = i+1) begin :mem
        if((8*i+8) > DW ) begin: last
          always @(posedge clk) begin
            if (wen[i]) begin
               mem_r[addr][DW-1:8*i] <= din[DW-1:8*i];
            end
          end
        end
        else begin: non_last
          always @(posedge clk) begin
            if (wen[i]) begin
               mem_r[addr][8*i+7:8*i] <= din[8*i+7:8*i];
            end
          end
        end
      end
    endgenerate

  wire [DW-1:0] dout_pre;
  assign dout_pre = mem_r[addr_r];

  generate
   if(FORCE_X2ZERO == 1) begin: force_x_to_zero
      for (i = 0; i < DW; i = i+1) begin:force_x_gen 
          `ifndef SYNTHESIS//{
         assign dout[i] = (dout_pre[i] === 1'bx) ? 1'b0 : dout_pre[i];
          `else//}{
         assign dout[i] = dout_pre[i];
          `endif//}
      end
   end
   else begin:no_force_x_to_zero
     assign dout = dout_pre;
   end
  endgenerate

endmodule

module adc_q_ram 
#(parameter DP = 8192,
  parameter FORCE_X2ZERO = 0,
  parameter DW = 24,
  parameter MW = 3,
  parameter AW = 13 
)
(
  input             clk, 
  input  [DW-1  :0] din, 
  input  [AW-1  :0] addr,
  input             cs,
  input             we,
  input  [MW-1:0]   wem,
  output [DW-1:0]   dout
);

    reg [DW-1:0] mem_r [0:DP-1];
    
    reg [AW-1:0] addr_r;
    wire [MW-1:0] wen;
    wire ren;

    assign ren = cs & (~we);
    assign wen = ({MW{cs & we}} & wem);

    always @(posedge clk)
    begin
        if (ren) begin
            addr_r <= addr;
        end
    end
    genvar i;
    generate
      for (i = 0; i < MW; i = i+1) begin :mem
        if((8*i+8) > DW ) begin: last
          always @(posedge clk) begin
            if (wen[i]) begin
               mem_r[addr][DW-1:8*i] <= din[DW-1:8*i];
            end
          end
        end
        else begin: non_last
          always @(posedge clk) begin
            if (wen[i]) begin
               mem_r[addr][8*i+7:8*i] <= din[8*i+7:8*i];
            end
          end
        end
      end
    endgenerate

  wire [DW-1:0] dout_pre;
  assign dout_pre = mem_r[addr_r];

  generate
   if(FORCE_X2ZERO == 1) begin: force_x_to_zero
      for (i = 0; i < DW; i = i+1) begin:force_x_gen 
          `ifndef SYNTHESIS//{
         assign dout[i] = (dout_pre[i] === 1'bx) ? 1'b0 : dout_pre[i];
          `else//}{
         assign dout[i] = dout_pre[i];
          `endif//}
      end
   end
   else begin:no_force_x_to_zero
     assign dout = dout_pre;
   end
  endgenerate
endmodule