`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/10/20 14:59:41
// Design Name: 
// Module Name: mdio_config
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mdio_config(
    input          sys_clk_200m,
    input          sys_rst,
    //MDIO接口
    output         eth_mdc  , //PHY管理接口的时钟信号
    inout          eth_mdio , //PHY管理接口的双向数据信号
    output         eth_rst_n, //以太网复位信号
    
    input          touch_key //触摸按键
    );
    
//wire define
wire          op_exec    ;  //触发开始信号
wire          op_rh_wl   ;  //低电平写，高电平读
wire  [4:0]   op_addr    ;  //寄存器地址
wire  [15:0]  op_wr_data ;  //写入寄存器的数据
wire          op_done    ;  //读写完成
wire  [15:0]  op_rd_data ;  //读出的数据
wire          op_rd_ack  ;  //读应答信号 0:应答 1:未应答
wire          dri_clk    ;  //驱动时钟
wire          sys_clk    ;
wire          sys_rst_n  ;
assign sys_rst_n = ~sys_rst;
clk_wiz_0 c0(
    .clk_in1(sys_clk_200m),
    .resetn(sys_rst_n),
    .locked(),
    .clk_out1(sys_clk)
);

//硬件复位
assign eth_rst_n = sys_rst_n;

//MDIO接口驱动
mdio_dri #(
    .PHY_ADDR    (5'b00111),    //PHY地址
    .CLK_DIV     (6'd10)     //分频系数
    )
    u_mdio_dri(
    .clk        (sys_clk   ),
    .rst_n      (sys_rst_n ),
    .op_exec    (op_exec   ),
    .op_rh_wl   (op_rh_wl  ),   
    .op_addr    (op_addr   ),   
    .op_wr_data (op_wr_data),   
    .op_done    (op_done   ),   
    .op_rd_data (op_rd_data),   
    .op_rd_ack  (op_rd_ack ),   
    .dri_clk    (dri_clk   ),  
                 
    .eth_mdc    (eth_mdc   ),   
    .eth_mdio   (eth_mdio  )   
);      

//MDIO接口读写控制    
mdio_ctrl  u_mdio_ctrl(
    .clk           (dri_clk   ),  
    .rst_n         (sys_rst_n ),  
    .soft_rst_trig (touch_key ),  
    .op_done       (op_done   ),  
    .op_rd_data    (op_rd_data),  
    .op_rd_ack     (op_rd_ack ),  
    .op_exec       (op_exec   ),  
    .op_rh_wl      (op_rh_wl  ),  
    .op_addr       (op_addr   ),  
    .op_wr_data    (op_wr_data),  
    .led           ()
);      

endmodule
