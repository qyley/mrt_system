`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/26 14:23:12
// Design Name: 
// Module Name: diffft_8192_b2_omega_ram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module diffft_8192_b2_omega_ram(
input             clk,
input             rstn,

input             rd_en,
input      [11:0] addr,

output reg [31:0] dout
    );

reg [31:0] mem [4095:0];

always@(posedge clk)
begin
    if(rd_en) begin
	    dout=mem[addr];
	end
	else begin
		dout=32'dz;
	end
end


initial $readmemh("D:/table/Project/VC707_mrt/system_mrt/data/Wn_8192_fix16_14.txt",mem);

endmodule

