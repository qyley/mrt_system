`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/27 16:45:30
// Design Name: 
// Module Name: cordic_myself_v3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/13 18:51:45
// Design Name: 
// Module Name: cordic_myself
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module cordic_myself_v3#(
parameter DIN_WIDTH=24
)(
input 					        	clk,
input 					        	rst_n,
input	signed [DIN_WIDTH-1:0]		x,
input	signed [DIN_WIDTH-1:0]	   	y,
input						        start,
output 	reg signed [DIN_WIDTH-1:0]	angle,
output 	reg  [DIN_WIDTH-1:0]	length,
output					        	finished
    );

// parameter angle_0 = 32'd2949120;     //45度*2^16
// parameter angle_1 = 32'd1740992;     //26.5651度*2^16
// parameter angle_2 = 32'd919872;      //14.0362度*2^16
// parameter angle_3 = 32'd466944;      //7.1250度*2^16
// parameter angle_4 = 32'd234368;      //3.5763度*2^16
// parameter angle_5 = 32'd117312;      //1.7899度*2^16
// parameter angle_6 = 32'd58688;       //0.8952度*2^16
// parameter angle_7 = 32'd29312;       //0.4476度*2^16
// parameter angle_8 = 32'd14656;       //0.2238度*2^16
// parameter angle_9 = 32'd7360;        //0.1119度*2^16
// parameter angle_10 = 32'd3648;       //0.0560度*2^16
// parameter angle_11 = 32'd1856;	     //0.0280度*2^16
// parameter angle_12 = 32'd896;        //0.0140度*2^16
// parameter angle_13 = 32'd448;        //0.0070度*2^16
// parameter angle_14 = 32'd256;        //0.0035度*2^16
// parameter angle_15 = 32'd128;        //0.0018度*2^16

parameter signed angle_pi = 27'd52707179;     //pi*2^24
parameter signed angle_fpi= ~angle_pi+1;     //-pi*2^24
parameter signed angle_0  = 27'd13176795;     //arctan(1/2^0)*2^24
parameter signed angle_1  = 27'd7778716;     //arctan(1/2^1)*2^24
parameter signed angle_2  = 27'd4110060;      //arctan(1/2^2)*2^24
parameter signed angle_3  = 27'd2086331;      //arctan(1/2^3)*2^24
parameter signed angle_4  = 27'd1047214;      //arctan(1/2^4)*2^24
parameter signed angle_5  = 27'd524117;      //arctan(1/2^5)*2^24
parameter signed angle_6  = 27'd262123;       //arctan(1/2^6)*2^24
parameter signed angle_7  = 27'd131069;       //arctan(1/2^7)*2^24
parameter signed angle_8  = 27'd65536;       //arctan(1/2^8)*2^24
parameter signed angle_9  = 27'd32768;        //arctan(1/2^9)*2^24
parameter signed angle_10 = 27'd16384;       //arctan(1/2^10)*2^24
parameter signed angle_11 = 27'd8192;	     //arctan(1/2^11)*2^24
parameter signed angle_12 = 27'd4096;        //arctan(1/2^12)*2^24
parameter signed angle_13 = 27'd2048;        //arctan(1/2^13)*2^24
parameter signed angle_14 = 27'd1024;        //arctan(1/2^14)*2^24
parameter signed angle_15 = 27'd512;        //arctan(1/2^15)*2^24
parameter signed angle_16 = 27'd256;       //arctan(1/2^8)*2^24
parameter signed angle_17 = 27'd128;        //arctan(1/2^9)*2^24
parameter signed angle_18 = 27'd64;       //arctan(1/2^10)*2^24
parameter signed angle_19 = 27'd32;	     //arctan(1/2^11)*2^24
parameter signed angle_20 = 27'd16;        //arctan(1/2^12)*2^24
parameter signed angle_21 = 27'd8;        //arctan(1/2^13)*2^24
parameter signed angle_22 = 27'd4;        //arctan(1/2^14)*2^24
parameter signed angle_23 = 27'd2;        //arctan(1/2^15)*2^24


parameter pipeline = 24;
parameter K = 32'h09b74;			//0.607253*2^16,


reg signed 	[DIN_WIDTH+pipeline:0] 		x0,y0;
reg signed 	[DIN_WIDTH+pipeline:0] 		x1,y1;
reg signed 	[DIN_WIDTH+pipeline:0] 		x2,y2;
reg signed 	[DIN_WIDTH+pipeline:0] 		x3,y3;
reg signed 	[DIN_WIDTH+pipeline:0] 		x4,y4;
reg signed 	[DIN_WIDTH+pipeline:0] 		x5,y5;
reg signed 	[DIN_WIDTH+pipeline:0] 		x6,y6;
reg signed 	[DIN_WIDTH+pipeline:0] 		x7,y7;
reg signed 	[DIN_WIDTH+pipeline:0] 		x8,y8;
reg signed 	[DIN_WIDTH+pipeline:0] 		x9,y9;
reg signed 	[DIN_WIDTH+pipeline:0] 		x10,y10;
reg signed 	[DIN_WIDTH+pipeline:0] 		x11,y11;
reg signed 	[DIN_WIDTH+pipeline:0] 		x12,y12;
reg signed 	[DIN_WIDTH+pipeline:0] 		x13,y13;
reg signed 	[DIN_WIDTH+pipeline:0] 		x14,y14;
reg signed 	[DIN_WIDTH+pipeline:0] 		x15,y15;
reg signed 	[DIN_WIDTH+pipeline:0] 		x16,y16;
reg signed 	[DIN_WIDTH+pipeline:0] 		x17,y17;
reg signed 	[DIN_WIDTH+pipeline:0] 		x18,y18;
reg signed 	[DIN_WIDTH+pipeline:0] 		x19,y19;
reg signed 	[DIN_WIDTH+pipeline:0] 		x20,y20;
reg signed 	[DIN_WIDTH+pipeline:0] 		x21,y21;
reg signed 	[DIN_WIDTH+pipeline:0] 		x22,y22;
reg signed 	[DIN_WIDTH+pipeline:0] 		x23,y23;
reg signed 	[DIN_WIDTH+pipeline:0] 		x24,y24;

reg signed  [26:0] z0,z1,z2,z3,z4,z5,z6,z7,z8,z9,z10,z11,z12,
                   z13,z14,z15,z16,z17,z18,z19,z20,z21,z22,z23,z24;

reg [1:0] s0;
reg [1:0] s1;
reg [1:0] s2;
reg [1:0] s3;
reg [1:0] s4;
reg [1:0] s5;
reg [1:0] s6;
reg [1:0] s7;
reg [1:0] s8;
reg [1:0] s9;
reg [1:0] s10;
reg [1:0] s11;
reg [1:0] s12;
reg [1:0] s13;
reg [1:0] s14;
reg [1:0] s15;
reg [1:0] s16;
reg [1:0] s17;
reg [1:0] s18;
reg [1:0] s19;
reg [1:0] s20;
reg [1:0] s21;
reg [1:0] s22;
reg [1:0] s23;
reg [1:0] s24;

reg  [4:0]           count;

always@(posedge clk or negedge rst_n)begin
	if(!rst_n)
		count <= 'b0;
	else if(start)begin
		if(count != pipeline+2)
			count <= count + 1'b1;
		else 
			count <= count;
	end
end
        xdelay_module #(
	     .D      (  26 ),
	     .W       (   1)
	      )
	    
   xdelay_module_inst (
      .i_clk(clk),      // input wire clk
      .i_rstn(rst_n),
      .s_i(start),      
      .s_o(finished) 
    );  
//assign finished = (count == pipeline+2)?1'b1:1'b0;

always@(posedge clk or negedge rst_n)begin
	if(!rst_n)begin
		x0 <= 'b0;
		y0 <= 'b0;
		z0 <= 'b0;
		s0 <= 'b0;
	end
	
	else begin
		if(x[DIN_WIDTH-1]) begin
		    x0 <= (~x+1)<<<pipeline;
		end
		else begin
		    x0 <= x<<<pipeline;
		end
		
		if(y[DIN_WIDTH-1]) begin
		    y0 <= (~y+1)<<<pipeline;
		end
		else begin
		    y0 <= y<<<pipeline;
		end
		
		z0 <= 'b0;
		s0 <= {x[DIN_WIDTH-1],y[DIN_WIDTH-1]};
	end
end 

always@(posedge clk or negedge rst_n)begin//第1次迭代
	if(!rst_n)begin
		x1 <= 'b0;
		y1 <= 'b0;
		z1 <= 'b0;
		s1 <= 'b0;
	end
	else if(!y0[DIN_WIDTH+pipeline]) begin//每次判断y坐标的正负决定旋转方向
		x1 <= x0 + y0;
		y1 <= y0 - x0;
		z1 <= z0 + angle_0;
		s1 <= s0;
	end
	else begin
		x1 <= x0 - y0;
		y1 <= y0 + x0;
		z1 <= z0 - angle_0;
		s1 <= s0;		
	end
end 

always@(posedge clk or negedge rst_n)begin//第2次迭代
	if(!rst_n)begin
		x2 <= 'b0;
		y2 <= 'b0;
		z2 <= 'b0;
		s2 <= 'b0;
	end
	else if(!y1[DIN_WIDTH+pipeline]) begin
		x2 <= x1 + (y1>>>1);
		y2 <= y1 - (x1>>>1);
		z2 <= z1 + angle_1;
		s2 <= s1;
	end
	else begin
		x2 <= x1 - (y1>>>1);
		y2 <= y1 + (x1>>>1);
		z2 <= z1 - angle_1;	
		s2 <= s1;
	end
end 

always@(posedge clk or negedge rst_n)begin//第3次迭代
	if(!rst_n)begin
		x3 <= 'b0;
		y3 <= 'b0;
		z3 <= 'b0;
		s3 <= 'b0;
	end
	else if(!y2[DIN_WIDTH+pipeline]) begin
		x3 <= x2 + (y2>>>2);
		y3 <= y2 - (x2>>>2);
		z3 <= z2 + angle_2;
		s3 <= s2;
	end
	else begin
		x3 <= x2 - (y2>>>2);
		y3 <= y2 + (x2>>>2);
		z3 <= z2 - angle_2;	
		s3 <= s2;
	end
end 

always@(posedge clk or negedge rst_n)begin//第4次迭代
	if(!rst_n)begin
		x4 <= 'b0;
		y4 <= 'b0;
		z4 <= 'b0;
		s4 <= 'b0;
	end
	else if(!y3[DIN_WIDTH+pipeline]) begin
		x4 <= x3 + (y3>>>3);
		y4 <= y3 - (x3>>>3);
		z4 <= z3 + angle_3;
		s4 <= s3;
	end
	else begin
		x4 <= x3 - (y3>>>3);
		y4 <= y3 + (x3>>>3);
		z4 <= z3 - angle_3;	
		s4 <= s3;
	end
end 

always@(posedge clk or negedge rst_n)begin//第5次迭代
	if(!rst_n)begin
		x5 <= 'b0;
		y5 <= 'b0;
		z5 <= 'b0;
		s5 <= 'b0;
	end
	else if(!y4[DIN_WIDTH+pipeline]) begin
		x5 <= x4 + (y4>>>4);
		y5 <= y4 - (x4>>>4);
		z5 <= z4 + angle_4;
		s5 <= s4;
	end
	else begin
		x5 <= x4 - (y4>>>4);
		y5 <= y4 + (x4>>>4);
		z5 <= z4 - angle_4;	
		s5 <= s4;
	end
end 

always@(posedge clk or negedge rst_n)begin//第6次迭代
	if(!rst_n)begin
		x6 <= 'b0;
		y6 <= 'b0;
		z6 <= 'b0;
		s6 <= 'b0;
	end
	else if(!y5[DIN_WIDTH+pipeline]) begin
		x6 <= x5 + (y5>>>5);
		y6 <= y5 - (x5>>>5);
		z6 <= z5 + angle_5;
		s6 <= s5;
	end
	else begin
		x6 <= x5 - (y5>>>5);
		y6 <= y5 + (x5>>>5);
		z6 <= z5 - angle_5;	
		s6 <= s5;
	end
end 

always@(posedge clk or negedge rst_n)begin//第7次迭代
	if(!rst_n)begin
		x7 <= 'b0;
		y7 <= 'b0;
		z7 <= 'b0;
		s7 <= 'b0;
	end
	else if(!y6[DIN_WIDTH+pipeline]) begin
		x7 <= x6 + (y6>>>6);
		y7 <= y6 - (x6>>>6);
		z7 <= z6 + angle_6;
		s7 <= s6;
	end
	else begin
		x7 <= x6 - (y6>>>6);
		y7 <= y6 + (x6>>>6);
		z7 <= z6 - angle_6;	
		s7 <= s6;
	end
end 

always@(posedge clk or negedge rst_n)begin//第8次迭代
	if(!rst_n)begin
		x8 <= 'b0;
		y8 <= 'b0;
		z8 <= 'b0;
		s8 <= 'b0;
	end
	else if(!y7[DIN_WIDTH+pipeline]) begin
		x8 <= x7 + (y7>>>7);
		y8 <= y7 - (x7>>>7);
		z8 <= z7 + angle_7;
		s8 <= s7;
	end
	else begin
		x8 <= x7 - (y7>>>7);
		y8 <= y7 + (x7>>>7);
		z8 <= z7 - angle_7;	
		s8 <= s7;
	end
end 

always@(posedge clk or negedge rst_n)begin//第9次迭代
	if(!rst_n)begin
		x9 <= 'b0;
		y9 <= 'b0;
		z9 <= 'b0;
		s9 <= 'b0;
	end
	else if(!y8[DIN_WIDTH+pipeline]) begin
		x9 <= x8 + (y8>>>8);
		y9 <= y8 - (x8>>>8);
		z9 <= z8 + angle_8;
		s9 <= s8;
	end
	else begin
		x9 <= x8 - (y8>>>8);
		y9 <= y8 + (x8>>>8);
		z9 <= z8 - angle_8;	
		s9 <= s8;
	end
end 

always@(posedge clk or negedge rst_n)begin//第10次迭代
	if(!rst_n)begin
		x10 <= 'b0;
		y10 <= 'b0;
		z10 <= 'b0;
		s10 <= 'b0;
	end
	else if(!y9[DIN_WIDTH+pipeline]) begin
		x10 <= x9 + (y9>>>9);
		y10 <= y9 - (x9>>>9);
		z10 <= z9 + angle_9;
		s10 <= s9;
	end
	else begin
		x10 <= x9 - (y9>>>9);
		y10 <= y9 + (x9>>>9);
		z10 <= z9 - angle_9;	
		s10 <= s9;
	end
end 

always@(posedge clk or negedge rst_n)begin//第11次迭代
	if(!rst_n)begin
		x11 <= 'b0;
		y11 <= 'b0;
		z11 <= 'b0;
		s11 <= 'b0;
	end
	else if(!y10[DIN_WIDTH+pipeline]) begin
		x11 <= x10 + (y10>>>10);
		y11 <= y10 - (x10>>>10);
		z11 <= z10 + angle_10;
		s11 <= s10;
	end
	else begin
		x11 <= x10 - (y10>>>10);
		y11 <= y10 + (x10>>>10);
		z11 <= z10 - angle_10;	
		s11 <= s10;
	end
end 

always@(posedge clk or negedge rst_n)begin//第12次迭代
	if(!rst_n)begin
		x12 <= 'b0;
		y12 <= 'b0;
		z12 <= 'b0;
		s12 <= 'b0;
	end
	else if(!y11[DIN_WIDTH+pipeline]) begin
		x12 <= x11 + (y11>>>11);
		y12 <= y11 - (x11>>>11);
		z12 <= z11 + angle_11;
		s12 <= s11;
	end
	else begin
		x12 <= x11 - (y11>>>11);
		y12 <= y11 + (x11>>>11);
		z12 <= z11 - angle_11;	
		s12 <= s11;
	end
end 

always@(posedge clk or negedge rst_n)begin//第13次迭代
	if(!rst_n)begin
		x13 <= 'b0;
		y13 <= 'b0;
		z13 <= 'b0;
		s13 <= 'b0;
	end
	else if(!y12[DIN_WIDTH+pipeline]) begin
		x13 <= x12 + (y12>>>12);
		y13 <= y12 - (x12>>>12);
		z13 <= z12 + angle_12;
		s13 <= s12;
	end
	else begin
		x13 <= x12 - (y12>>>12);
		y13 <= y12 + (x12>>>12);
		z13 <= z12 - angle_12;	
		s13 <= s12;
	end
end 

always@(posedge clk or negedge rst_n)begin//第14次迭代
	if(!rst_n)begin
		x14 <= 'b0;
		y14 <= 'b0;
		z14 <= 'b0;
		s14 <= 'b0;
	end
	else if(!y13[DIN_WIDTH+pipeline]) begin
		x14 <= x13 + (y13>>>13);
		y14 <= y13 - (x13>>>13);
		z14 <= z13 + angle_13;
		s14 <= s13;
	end
	else begin
		x14 <= x13 - (y13>>>13);
		y14 <= y13 + (x13>>>13);
		z14 <= z13 - angle_13;	
		s14 <= s13;
	end
end 

always@(posedge clk or negedge rst_n)begin//第15次迭代
	if(!rst_n)begin
		x15 <= 'b0;
		y15 <= 'b0;
		z15 <= 'b0;
		s15 <= 'b0;
	end
	else if(!y14[DIN_WIDTH+pipeline]) begin
		x15 <= x14 + (y14>>>14);
		y15 <= y14 - (x14>>>14);
		z15 <= z14 + angle_14;
		s15 <= s14;
	end
	else begin
		x15 <= x14 - (y14>>>14);
		y15 <= y14 + (x14>>>14);
		z15 <= z14 - angle_14;	
		s15 <= s14;
	end
end 

always@(posedge clk or negedge rst_n)begin//第16次迭代
	if(!rst_n)begin
		x16 <= 'b0;
		y16 <= 'b0;
		z16 <= 'b0;
		s16 <= 'b0;
	end
	else if(!y15[DIN_WIDTH+pipeline]) begin
		x16 <= x15 + (y15>>>15);
		y16 <= y15 - (x15>>>15);
		z16 <= z15 + angle_15;
		s16 <= s15;
	end
	else begin
		x16 <= x15 - (y15>>>15);
		y16 <= y15 + (x15>>>15);
		z16 <= z15 - angle_15;
		s16 <= s15;		
	end
end

always@(posedge clk or negedge rst_n)begin//第17次迭代
	if(!rst_n)begin
		x17 <= 'b0;
		y17 <= 'b0;
		z17 <= 'b0;
		s17 <= 'b0;
	end
	else if(!y16[DIN_WIDTH+pipeline]) begin
		x17 <= x16 + (y16>>>16);
		y17 <= y16 - (x16>>>16);
		z17 <= z16 + angle_16;
		s17 <= s16;
	end
	else begin
		x17 <= x16 - (y16>>>16);
		y17 <= y16 + (x16>>>16);
		z17 <= z16 - angle_16;
	    s17 <= s16;
	end
end

always@(posedge clk or negedge rst_n)begin//第18次迭代
	if(!rst_n)begin
		x18 <= 'b0;
		y18 <= 'b0;
		z18 <= 'b0;
		s18 <= 'b0;
	end
	else if(!y17[DIN_WIDTH+pipeline]) begin
		x18 <= x17 + (y17>>>17);
		y18 <= y17 - (x17>>>17);
		z18 <= z17 + angle_17;
		s18 <= s17;
	end
	else begin
		x18 <= x17 - (y17>>>17);
		y18 <= y17 + (x17>>>17);
		z18 <= z17 - angle_17;	
		s18 <= s17;
	end
end

always@(posedge clk or negedge rst_n)begin//第19次迭代
	if(!rst_n)begin
		x19 <= 'b0;
		y19 <= 'b0;
		z19 <= 'b0;
		s19 <= 'b0;
	end
	else if(!y18[DIN_WIDTH+pipeline]) begin
		x19 <= x18 + (y18>>>18);
		y19 <= y18 - (x18>>>18);
		z19 <= z18 + angle_18;
		s19 <= s18;
	end
	else begin
		x19 <= x18 - (y18>>>18);
		y19 <= y18 + (x18>>>18);
		z19 <= z18 - angle_18;	
		s19 <= s18;
	end
end

always@(posedge clk or negedge rst_n)begin//第20次迭代
	if(!rst_n)begin
		x20 <= 'b0;
		y20 <= 'b0;
		z20 <= 'b0;
		s20 <= 'b0;
	end
	else if(!y19[DIN_WIDTH+pipeline]) begin
		x20 <= x19 + (y19>>>19);
		y20 <= y19 - (x19>>>19);
		z20 <= z19 + angle_18;
		s20 <= s19;
	end
	else begin
		x20 <= x19 - (y19>>>19);
		y20 <= y19 + (x19>>>19);
		z20 <= z19 - angle_19;	
		s20 <= s19;
	end
end

always@(posedge clk or negedge rst_n)begin//第21次迭代
	if(!rst_n)begin
		x21 <= 'b0;
		y21 <= 'b0;
		z21 <= 'b0;
		s21 <= 'b0;
	end
	else if(!y20[DIN_WIDTH+pipeline]) begin
		x21 <= x20 + (y20>>>20);
		y21 <= y20 - (x20>>>20);
		z21 <= z20 + angle_20;
		s21 <= s20;
	end
	else begin
		x21 <= x20 - (y20>>>20);
		y21 <= y20 + (x20>>>20);
		z21 <= z20 - angle_20;	
		s21 <= s20;
	end
end

always@(posedge clk or negedge rst_n)begin//第22次迭代
	if(!rst_n)begin
		x22 <= 'b0;
		y22 <= 'b0;
		z22 <= 'b0;
		s22 <= 'b0;
	end
	else if(!y21[DIN_WIDTH+pipeline]) begin
		x22 <= x21 + (y21>>>21);
		y22 <= y21 - (x21>>>21);
		z22 <= z21 + angle_21;
		s22 <= s21;
	end
	else begin
		x22 <= x21 - (y21>>>21);
		y22 <= y21 + (x21>>>21);
		z22 <= z21 - angle_21;
		s22 <= s21;
	end
end

always@(posedge clk or negedge rst_n)begin//第23次迭代
	if(!rst_n)begin
		x23 <= 'b0;
		y23 <= 'b0;
		z23 <= 'b0;
		s23 <= 'b0;
	end
	else if(!y22[DIN_WIDTH+pipeline]) begin
		x23 <= x22 + (y22>>>22);
		y23 <= y22 - (x22>>>22);
		z23 <= z22 + angle_22;
		s23 <= s22;
	end
	else begin
		x23 <= x22 - (y22>>>22);
		y23 <= y22 + (x22>>>22);
		z23 <= z22 - angle_22;
        s23 <= s22;		
	end
end

always@(posedge clk or negedge rst_n)begin//第24次迭代
	if(!rst_n)begin
		x24 <= 'b0;
		y24 <= 'b0;
		z24 <= 'b0;
		s24 <= 'b0;
	end
	else if(!y23[DIN_WIDTH+pipeline]) begin
		x24 <= x23 + (y23>>>23);
		y24 <= y23 - (x23>>>23);
		z24 <= z23 + angle_23;
		s24 <= s23;
	end
	else begin
		x24 <= x23 - (y23>>>23);
		y24 <= y23 + (x23>>>23);
		z24 <= z23 - angle_23;	
		s24 <= s23;
	end
end

wire [26:0] angle_tmp;
assign angle_tmp=(s24==0)?z24://第一象限
                 (s24==1)?(~z24+1)://第四象限
				 (s24==2)?(angle_pi-z24)://第二象限
				          (angle_fpi+z24);//第三象限


wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp1;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp4;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp5;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp7;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp8;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp10;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp11;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp12;
wire[DIN_WIDTH+pipeline-1:0]  mozhi_tmp14;

assign mozhi_tmp1  = x24>>>1;
assign mozhi_tmp4  = x24>>>4;
assign mozhi_tmp5  = x24>>>5;
assign mozhi_tmp7  = x24>>>7;
assign mozhi_tmp8  = x24>>>8;
assign mozhi_tmp10  = x24>>>10;
assign mozhi_tmp11  = x24>>>11;
assign mozhi_tmp12  = x24>>>12;
assign mozhi_tmp14  = x24>>>14;
assign mozhi_tmp  = mozhi_tmp1+mozhi_tmp4+mozhi_tmp5+mozhi_tmp7+mozhi_tmp8+
                    mozhi_tmp10+mozhi_tmp11+mozhi_tmp12+mozhi_tmp14;


always@(posedge clk or negedge rst_n)begin
	if(!rst_n)begin
		length <= 'b0;
		angle <= 'b0;
	end
	else begin
        length 	<= mozhi_tmp>>>24;
        angle   <= angle_tmp[26:3];//fix27_24→fix24_21
	end
end 




endmodule