`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/26 14:22:30
// Design Name: 
// Module Name: diffft_8192_b2_data_ram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module diffft_8192_b2_data_ram(
input             clk,
input             rstn,

input             wr_en,
input             rd_en,
input      [12:0] addr,
input      [79:0] din,//fix40_23

output reg [79:0] dout
    );

reg [79:0] mem [8191:0];

always@(posedge clk)
begin
	if(wr_en) begin
	    mem[addr]<=din;
	end
	else if(rd_en) begin
	    dout<=mem[addr];
	end
	else
	    dout<=80'bz;
end



endmodule