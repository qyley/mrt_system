`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/05 23:39:49
// Design Name: 
// Module Name: cascade_compare
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module cascade_compare #(parameter W = 7,L = 1,DATA_BITS = 8)(
    input clk,
    input rstn,
    input [DATA_BITS-1:0] d,
    output [DATA_BITS-1:0] out
);
    
    wire [W*L*DATA_BITS-1:0] q;
    
    my_compare #(.DATA_BITS(DATA_BITS)) u0(
        .i_clk (clk),
        .i_rstn (rstn),
        .phase_unwrap   (d),
        .phase_unwrap_norm   (q[DATA_BITS-1:0])
    );
               
    genvar i;
    generate for(i=1;i<W*L;i=i+1) begin:dff_chain
        my_compare #(.DATA_BITS(DATA_BITS)) md8(
            .i_clk (clk),
            .i_rstn (rstn),
            .phase_unwrap   (q[i*DATA_BITS-1:(i-1)*DATA_BITS]),
            .phase_unwrap_norm  (q[(i+1)*DATA_BITS-1:i*DATA_BITS])
        );
        end
    endgenerate
    

         assign out = q[W*DATA_BITS-1:(W-1)*DATA_BITS];


endmodule