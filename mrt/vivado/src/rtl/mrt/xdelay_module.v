`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/13 19:12:52
// Design Name: 
// Module Name: xdelay_module
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module xdelay_module#(
    parameter D = 5,
    parameter W = 4
)(
    input i_clk,
    input i_rstn,
    input [W-1:0] s_i,
    output reg  [W-1:0] s_o
);
       reg      [W-1:0]       sreg [D:0] ;
    
    
    always@ (posedge i_clk)
    if (!i_rstn)
    begin
      sreg[0]<=0;
      s_o<=0;
    end
    else
    begin
      sreg[0]<=s_i;
      s_o<=sreg[D-2];
    end
    
    
    genvar     i ;
    generate
        for(i=1; i<=D-2; i=i+1) begin: delay_chain
    begin 
   always @(posedge i_clk ) begin
        if (!i_rstn) begin
       sreg[i]<=0;
        end
        else begin
        sreg[i]<=sreg[i-1];
        end
        end
        end
        end
    endgenerate   

endmodule
