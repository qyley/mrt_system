`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/14 17:52:58
// Design Name: 
// Module Name: Get_sigma_dp_v3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Get_sigma_dp_v3#(
     parameter DATA_IN_WIDTH      = 24       ,
	 parameter DATA_OUT_WIDTH     = 32       ,
	 parameter COMPUTE_LEN_WIDTH  = 14    
	 
   )(
    input i_clk,
    input i_rstn,

    input  i_intensity_flag,
    input [COMPUTE_LEN_WIDTH-1:0] i_intensity_count,
    input signed [DATA_IN_WIDTH-1:0]  i_phase_data,   //21bit fra
    input  i_phase_data_valid,
    input  i_phase_data_last,
    output [DATA_OUT_WIDTH-1 : 0] sigma_dp_data,    ////fra 25bit
    output  sigma_dp_data_valid
    
    );
    
    
    wire [2*DATA_IN_WIDTH-1:0] i_phase_data_square;
    wire  i_phase_data_square_valid;
//    mult_phase mult_phase_inst (
//      .CLK(i_clk),  // input wire CLK
//      .A(i_phase_data),      // input wire [11 : 0] A
//      .B(i_phase_data),      // input wire [11 : 0] B
//      .P(i_phase_data_square)      // output wire [23 : 0] P
//    );
    
  
    mult_myself #(
	     .N          (  24  ) ,
	     .M          (  24  )
	     ) 
    mult_myself2_inst (
      .clk(i_clk),                      // input wire clk
      .rstn(i_rstn),
      .data_rdy(i_phase_data_valid),      // 
      .mult1(i_phase_data),        //
      .mult2(i_phase_data),                  // 
      .res_rdy(i_phase_data_square_valid),            // 
      .res(i_phase_data_square)
    
    ); 
  
  
    
   
    wire  signed [DATA_IN_WIDTH-1:0]  i_phase_data_d24;
    wire                       i_intensity_flag_d24;
    wire [COMPUTE_LEN_WIDTH-1:0] i_intensity_count_d25;//为了同步求得的相位和

    wire                       i_phase_data_last_d24;
    
    
    xdelay_module #(
	     .D      ( 24 ),
	     .W       (   1)
	      )
	    
   xdelay_module_intensity_flag_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(i_intensity_flag),      
      .s_o(i_intensity_flag_d24) 
    );   
    
     
 reg     i_intensity_flag_d25;
      always @(posedge i_clk) begin
         if(~i_rstn) begin
             i_intensity_flag_d25  <= 0;
        
         end
         else begin
             i_intensity_flag_d25 <= i_intensity_flag_d24;
         
         end
  end
   xdelay_module #(
	     .D      (  25 ),
	     .W       (   14)
	      )
	    
   xdelay_module_intensity_count_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(i_intensity_count),      
      .s_o(i_intensity_count_d25) 
    );  
    
    
       xdelay_module #(
	     .D      (  24 ),
	     .W       (  1)
	      )
	    
   xdelay_module_last2_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(i_phase_data_last),      
      .s_o(i_phase_data_last_d24) 
    ); 
    
    
     xdelay_module #(
	     .D      (  24 ),
	     .W       (  24)
	      )
	    
   xdelay_module_phase_data_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(i_phase_data),      
      .s_o(i_phase_data_d24) 
    ); 
       
    
    wire o_sum_p_vld;
           
           reg signed  [DATA_IN_WIDTH-1+13:0] sum_phase;//24+13? 37bit  fra 21bit
           reg  [DATA_IN_WIDTH*2-1+13:0] sum_phase_square;//48+13? 61bit  fra 42bit
           
    
                   reg sun_p_rst;
                   
                   always @(posedge i_clk) begin
                       if(sun_p_rst||~i_rstn) begin sum_phase <= 0; sum_phase_square<= 0; end
                       else if(i_intensity_flag_d24) begin  sum_phase <= sum_phase + i_phase_data_d24; sum_phase_square <= sum_phase_square + i_phase_data_square;end
                       else begin  sum_phase <= sum_phase;  sum_phase_square <= sum_phase_square; end    
                   end
  wire  signed  [DATA_IN_WIDTH-1:0] sum_phase_cut;
  assign sum_phase_cut=sum_phase[DATA_IN_WIDTH+9:10];  //1bit sign 12bit int 11bit fra
  wire    [DATA_IN_WIDTH*2+7:0] sum_phase_square_cut;
  assign sum_phase_square_cut=sum_phase_square[DATA_IN_WIDTH*2+7:0]; // 56bit  fra 42bit
  
  
//                    integer fp5;
//                    integer i;
//                    initial begin
                 
//                     fp5 = $fopen("sum_phase31_27bit.txt");                   
//                     i=0;
//                     end
//                   always @(posedge i_clk) begin
//                      if(i>=4147) begin
//                          $fclose(fp5);
//                     end
//                       else if(i_intensity_flag_d25) begin
//                          i <= i+1;
//                          $fdisplay(fp5,"%d",sum_phase);
//                       end
//                   end  
                   
                   
  
  wire [47:0]  mult_sum_phase ;//26bit int 22bit fra
  wire [69 : 0] mult_sum_phase_square;
  wire  [47 : 0] mult_sum_phase_square_cut,mult_sum_phase_square_cut_d10;
   wire  [47 : 0] sum_phase_sub;                                   
// mult_gen_0 mult_gen_0_inst (
//                     .CLK(i_clk),  // input wire CLK
//                     .A(sum_phase_cut),      // input wire [15 : 0] A
//                     .B(sum_phase_cut),      // input wire [15 : 0] B
//                     .P(mult_sum_phase)      // output wire [31 : 0] P
//                   );
 wire  mult_sum_phase_valid;
    mult_myself #(
	     .N          (  24  ) ,
	     .M          (  24  )
	     ) 
    mult_myself3_inst (
      .clk(i_clk),                      // input wire clk
      .rstn(i_rstn),
      .data_rdy(o_sum_p_vld),      // 
      .mult1(sum_phase_cut),        //
      .mult2(sum_phase_cut),                  // 
      .res_rdy(mult_sum_phase_valid),            // 
      .res(mult_sum_phase)
    
    ); 
    
    wire mult_sum_phase_square_valid;
       mult_myself_unsign #(
	     .N          (  56  ) ,
	     .M          (  14  )
	     ) 
    mult_myself_unsign2_inst (
      .clk(i_clk),                      // input wire clk
      .rstn(i_rstn),
      .data_rdy(o_sum_p_vld),      // 
      .mult1(sum_phase_square_cut),        //
      .mult2(i_intensity_count_d25),                  // 
      .res_rdy(mult_sum_phase_square_valid),            // 
      .res(mult_sum_phase_square)
    
    );  
             
//  mult_C mult_C_inst (
//                     .CLK(i_clk),  // input wire CLK
//                     .A(i_intensity_count_d5),      // input wire [13 : 0] A
//                     .B(sum_phase_square_cut),      // input wire [27 : 0] B
//                     .P(mult_sum_phase_square)      // output wire [41 : 0] P
//                   );
  assign     mult_sum_phase_square_cut= mult_sum_phase_square[67:20];/////fra  22bit
  
  
       xdelay_module #(
	     .D      (  10 ),
	     .W       (  48)
	      )
	    
   xdelay_module_mult_sum_phase_square_cut_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(mult_sum_phase_square_cut),      
      .s_o(mult_sum_phase_square_cut_d10) 
    ); 
  
  
//  assign    sum_phase_sub=mult_sum_phase_square_cut-mult_sum_phase;//22bit fra
  assign    sum_phase_sub=mult_sum_phase_square_cut_d10-mult_sum_phase;//22bit fra
  
  
// reg o_sum_p_vld_d1,o_sum_p_vld_d2,o_sum_p_vld_d3,o_sum_p_vld_d4;
 
//  always @(posedge i_clk) 
//         begin
//             if(!i_rstn)
//                 begin
//                     o_sum_p_vld_d1<=0;
//                     o_sum_p_vld_d2<=0;
//                     o_sum_p_vld_d3<=0;
//                     o_sum_p_vld_d4<=0;
              
//                 end
//             else 
//                 begin
//                     o_sum_p_vld_d1 <= o_sum_p_vld    ;
//                     o_sum_p_vld_d2 <= o_sum_p_vld_d1 ;
//                     o_sum_p_vld_d3 <= o_sum_p_vld_d2 ;
//                     o_sum_p_vld_d4<=o_sum_p_vld_d3;                  
//                 end
//         end  
         
 reg [COMPUTE_LEN_WIDTH-1:0] hold_count;
         
   always @(posedge i_clk) 
        begin
            if(!i_rstn)
                begin
                    hold_count<=0;           
                end
            else if (o_sum_p_vld)
                begin
                   hold_count<=i_intensity_count_d25;              
                end
                else hold_count<=hold_count;
        end 
 wire [23:0] sum_phase_sub_root;//////////////fra 11bit
 wire sum_phase_sub_root_valid;
// wire[31:0] sum_phase_su;
// wire sum_phase_su_valid;
// assign sum_phase_su_valid=o_sum_p_vld_d4;
// assign sum_phase_su=sum_phase_sub;
      sqrt_module #(
	     .d_width      (  48 ) ,
	     .q_width      (  23 ) ,
	     .r_width      (  24 )
	     ) 
    sqrt_module_inst (
      .clk(i_clk),      // input wire clk
      .rst(~i_rstn),
      .i_vaild(mult_sum_phase_valid),      
      .data_i(sum_phase_sub),  
      .o_vaild(sum_phase_sub_root_valid),  
      .data_o(sum_phase_sub_root),  
      .data_r()
    );

// get_root get_root_inst (
//    .aclk(i_clk),                                        // input wire aclk
//    .s_axis_cartesian_tvalid(o_sum_p_vld_d4),  // input wire s_axis_cartesian_tvalid
//    .s_axis_cartesian_tdata(sum_phase_sub),    // input wire [31 : 0] s_axis_cartesian_tdata
//    .m_axis_dout_tvalid(sum_phase_sub_root_valid),            // output wire m_axis_dout_tvalid
//    .m_axis_dout_tdata(sum_phase_sub_root)              // output wire [31 : 0] m_axis_dout_tdata
//  );  
    
 wire  [37 : 0] o_sigma_dp_data;///////frac   11+14 25bit
// wire sigma_dp_data_valid;
  
// div_get_sigma div_get_sigma_inst (
//    .aclk(i_clk),                                      // input wire aclk
//    .s_axis_divisor_tvalid(sum_phase_sub_root_valid),    // input wire s_axis_divisor_tvalid
//    .s_axis_divisor_tdata({2'b0,hold_count}),      // input wire [15 : 0] s_axis_divisor_tdata
//    .s_axis_dividend_tvalid(sum_phase_sub_root_valid),  // input wire s_axis_dividend_tvalid
//    .s_axis_dividend_tdata({8'b0,sum_phase_sub_root}),    // input wire [23 : 0] s_axis_dividend_tdata
//    .m_axis_dout_tvalid(sigma_dp_data_valid),          // output wire m_axis_dout_tvalid
//    .m_axis_dout_tdata(o_sigma_dp_data)            // output wire [31 : 0] m_axis_dout_tdata
//  );   
  
  
       divider_myself #(
	     .N          (  24+14  ) ,
	     .M          (  14  ) ,
	     .N_ACT      (  51  )
	     ) 
    divider_myself2_inst (
      .clk(i_clk),                      // input wire clk
      .rstn(i_rstn),
      .data_rdy(sum_phase_sub_root_valid),      // 
      .dividend({sum_phase_sub_root,14'b0}),        //
      .divisor(hold_count),                  // 
      .res_rdy(sigma_dp_data_valid),            // 
      .merchant(o_sigma_dp_data),           // output wire full
      .remainder() 
    
    );
  
  
  
 assign  sigma_dp_data=   o_sigma_dp_data [35:0] ; //fra 25bit
     //#########################control##########################################//////              
                   reg state,next_state;
                   reg sum_p_vld;
                   localparam Init=1'b0, Start=1'b1;
                   always @(*) begin
                       case(state)
                           Init: begin
                               if(~i_rstn) next_state = Init;
                               else next_state = Start;
                           end
                           Start: begin
                               if(~i_rstn) next_state = Init;
                               else next_state = Start;
                           end
                       endcase
                   end
                   
                   // State transfer block
                   always @(posedge i_clk) begin
                       if(~i_rstn) state <= Init;
                       else state <= next_state;
                   end
                   
               
                   // State output
                   always @(posedge i_clk) begin
                       // in default we disable and reset all counter
                       sun_p_rst     <= 1;
                       sum_p_vld     <= 0; // output data is invalid
                       case(next_state)
                           Init: ; // do as the default
                           Start: begin
                               sum_p_vld <= i_phase_data_last_d24;
                               sun_p_rst <= i_phase_data_last_d24;
                               end
                       endcase
                   end
                   
                   assign o_sum_p_vld = sum_p_vld;
    
    
    endmodule