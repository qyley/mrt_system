`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/13 17:30:04
// Design Name: 
// Module Name: Get_real_phase_v2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module Get_real_phase_v2#(
     parameter DATA_IN_WIDTH      = 24     ,
	 parameter COMPUTE_LEN_WIDTH  = 14     ,
	 parameter DATA_OUT_WIDTH     = 24
	 
   )(
   input                        i_clk                    ,
   input                        i_rstn                   ,

 
   input                        i_Data_In_Valid          ,
   input  [2*DATA_IN_WIDTH-1:0] i_Data_In                ,
   input                        i_Data_In_Last           ,

   output  reg                        o_Real_Phase_Valid    ,
   output  reg   [DATA_OUT_WIDTH-1:0]  o_Real_Data           ,
   output  reg  signed  [DATA_OUT_WIDTH-1:0]  o_Phase_Data          ,// //////fra 21bit
   output  reg                        o_Real_Phase_Last   
   
 
    );

 	reg [DATA_OUT_WIDTH-1:0] Data_re ;
    reg [DATA_OUT_WIDTH-1:0] Data_im ;
     
    

     reg i_data_vld_d1;


     always @(posedge i_clk) begin
         if(~i_rstn) begin
             i_data_vld_d1  <= 0;
             Data_re<=0;
              Data_im<=0;  
        
         end
         else begin
             i_data_vld_d1 <= i_Data_In_Valid;
		     Data_re   <= i_Data_In[DATA_OUT_WIDTH-1:0] ;
             Data_im   <= i_Data_In[2*DATA_OUT_WIDTH-1:DATA_OUT_WIDTH] ;          
         end
  end
  

    
    
    wire   [2*DATA_OUT_WIDTH-1:0]   m_axis_dout_tdata;
    wire                         m_axis_dout_tvalid;
    wire                         i_data_last;
    
    
     wire [DATA_OUT_WIDTH-1:0] Real_Data,Phase_Data;
     wire Phase_Real_Data_valid;
     
    cordic_myself_v3 #(
	     .DIN_WIDTH      (  DATA_IN_WIDTH ) )
	    
   cordic_myself_inst (
      .clk(i_clk),      // input wire clk
      .rst_n(i_rstn),
      .x(Data_re),      
      .y(Data_im),  
      .start(i_data_vld_d1),  
      .angle(Phase_Data),  
      .length(Real_Data),
      .finished(Phase_Real_Data_valid)
    );  
//  get_module_phase get_module_phase_inst (
//      .aclk(i_clk),  
//     .s_axis_cartesian_tvalid(i_data_vld_d1),  // input wire s_axis_cartesian_tvalid
//     .s_axis_cartesian_tdata({Data_im,Data_re}),    // input wire [63 : 0] s_axis_cartesian_tdata
//     .m_axis_dout_tvalid(m_axis_dout_tvalid),            // output wire m_axis_dout_tvalid
//     .m_axis_dout_tdata(m_axis_dout_tdata)              // output wire [95 : 0] m_axis_dout_tdata
//   );
   

//  wire [DATA_OUT_WIDTH-1:0] Real_Data,Phase_Data;
// assign  Real_Data =m_axis_dout_tdata[DATA_OUT_WIDTH-1:0] ;
// assign  Phase_Data=m_axis_dout_tdata[2*DATA_OUT_WIDTH-1:DATA_OUT_WIDTH] ;
 
    xdelay_module #(
	     .D      (  27 ),
	     .W       (   1)
	      )
	    
   xdelay_module_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(i_Data_In_Last),      
      .s_o(i_data_last) 
    );  

         always @(*) 

                               begin   
                                              o_Real_Phase_Valid=Phase_Real_Data_valid;
                                               o_Real_Phase_Last=i_data_last;
                                               o_Real_Data=Real_Data;
                                               o_Phase_Data=Phase_Data;
                              end 

            


endmodule