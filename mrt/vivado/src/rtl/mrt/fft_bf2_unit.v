`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/26 14:24:08
// Design Name: 
// Module Name: fft_bf2_unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fft_bf2_unit#(
parameter WORDDLENGTH = 80,
          WORDWLENGTH = 32)(
input                        clk,
input                        rstn,

input  signed    [WORDDLENGTH-1:0] data_din,//fix40_22
input  signed    [WORDWLENGTH-1:0] omega_din,//fix16_14
input                              din_valid,

output           [WORDDLENGTH-1:0] fft_bf2_dout
    );

//基2蝶形运算输入
reg  signed [(WORDDLENGTH>>1)-1:0] x1_r;
reg  signed [(WORDDLENGTH>>1)-1:0] x1_i;
wire signed [(WORDDLENGTH>>1)-1:0] x2_r;
wire signed [(WORDDLENGTH>>1)-1:0] x2_i;
wire signed [(WORDWLENGTH>>1)-1:0] wn_r;
wire signed [(WORDWLENGTH>>1)-1:0] wn_i;
assign {x2_r,x2_i}=data_din;
assign {wn_r,wn_i}=omega_din;

//基2蝶形运算输出
reg signed [((WORDDLENGTH+WORDWLENGTH)>>1)-1:0] x3_r;
reg signed [((WORDDLENGTH+WORDWLENGTH)>>1)-1:0] x3_i;
reg signed [((WORDDLENGTH+WORDWLENGTH)>>1)-1:0] x4_r;
reg signed [((WORDDLENGTH+WORDWLENGTH)>>1)-1:0] x4_i;
reg                                             x34_ready;

always@(posedge clk)
begin
    if(!rstn) begin
	    {x1_r,x1_i}<=0;
		//{x2_r,x2_i}<=0;
		//{wn_r,wn_i}<=0;
	end
	else begin
	    {x1_r,x1_i}<=data_din;
	    //{x2_r,x2_i}<={x1_r,x1_i};
	    //{wn_r,wn_i}<=omega_din;
	end
end


always@(posedge clk)
begin
    if(!rstn) begin
	    {x3_r,x3_i}<=0;
		{x4_r,x4_i}<=0;
	end
	else begin
	    if(din_valid) begin
	        x3_r<=(x1_r+x2_r);//fix40_22+fix40_22=fix40_22
		    x3_i<=(x1_i+x2_i);
			x4_r<=(x1_r-x2_r)*wn_r-(x1_i-x2_i)*wn_i;//fix40_22*fix16_14=fix56_36
		    x4_i<=(x1_r-x2_r)*wn_i+(x1_i-x2_i)*wn_r;
			x34_ready<=1;
		end
		else begin
		    x3_r<=x3_r;
			x3_i<=x3_i;
			x4_r<=x4_r;
			x4_i<=x4_i;
			x34_ready<=0;
		end
	end
end


assign fft_bf2_dout=(x34_ready==1)?({x3_r[39:0],x3_i[39:0]}):({x4_r[53:14],x4_i[53:14]});//fix40_22

endmodule
