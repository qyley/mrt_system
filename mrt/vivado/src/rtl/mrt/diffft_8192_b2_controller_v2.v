`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/27 10:05:01
// Design Name: 
// Module Name: diffft_8192_b2_controller_v2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module diffft_8192_b2_controller_v2(
input             clk,
input             rstn,

input             din_valid,
input             din_last,
output reg        din_ready,

output reg        data_ram_wr_en,
output reg        data_ram_rd_en,
output reg [12:0] data_ram_addr,
output reg        omega_ram_rd,
output reg [11:0] omega_ram_addr,
output reg        bf_din_valid,
output reg        fft_dout_reorder,
output reg        fft_dout_valid,
output reg        fft_dout_last,
output reg        error
    );

reg din_valid_d1;
reg din_last_d1;
always@(posedge clk)
begin
    if(!rstn) begin
	    din_valid_d1<=0;
		din_last_d1<=0;
	end
	else begin
	    din_valid_d1<=din_valid;
		din_last_d1<=din_last;
	end
end


//“级“循环参数及状态
parameter IDLE=4'd13;
parameter DATA_IN=4'd14;
parameter DATA_OUT=4'd15;
parameter L1_BF=4'd0;
parameter L2_BF=4'd1;
parameter L3_BF=4'd2;
parameter L4_BF=4'd3;
parameter L5_BF=4'd4;
parameter L6_BF=4'd5;
parameter L7_BF=4'd6;
parameter L8_BF=4'd7;
parameter L9_BF=4'd8;
parameter L10_BF=4'd9;
parameter L11_BF=4'd10;
parameter L12_BF=4'd11;
parameter L13_BF=4'd12;

//状态机写入信号延迟两拍
//last到来时正在写入第8191（对应存储地址8190）个数据
parameter DATA_NUM=16'd8191;

reg [3:0] level_current_state;
reg [3:0] level_next_state;

//”组“循环参数及状态
parameter BF_IDLE=3'd0;
parameter BF_rd_d1=3'd1;
parameter BF_rd_d2=3'd2;
parameter BF_js=3'd3;
parameter BF_wb_d1=3'd4;
parameter BF_wb_d2=3'd5;

parameter BF_dout=3'd6;

reg [2:0] bf2_state;

reg [15:0] data_cnt;
reg [15:0] group_cycle_cnt;
reg [15:0] interval_1;
reg [15:0] interval_2;

always@(posedge clk)
begin
    if(!rstn) begin
	    level_current_state<=IDLE;
	end
	else begin
	    level_current_state<=level_next_state;
	end
end


always@(posedge clk)
begin
    if(!rstn) begin
	    level_next_state<=IDLE;
		error<=0;
	end
	else begin
	    case(level_current_state)
		IDLE: begin
		    error<=0;
			
			if(din_valid&&(!din_last))
			    level_next_state<=DATA_IN;
			else
			    level_next_state<=level_next_state;
		end
		DATA_IN:
		    if(din_last_d1&&din_valid_d1)
			    if(data_cnt==DATA_NUM)
				    level_next_state<=L1_BF;
				else begin
				    level_next_state<=IDLE;
					error<=1;
				end
			else
			    level_next_state<=level_next_state;
		L1_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L2_BF;
		    else
			    level_next_state<=level_next_state;
		L2_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L3_BF;
		    else
			    level_next_state<=level_next_state;
		L3_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L4_BF;
		    else
			    level_next_state<=level_next_state;
		L4_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L5_BF;
		    else
			    level_next_state<=level_next_state;
		L5_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L6_BF;
		    else
			    level_next_state<=level_next_state;
		L6_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L7_BF;
		    else
			    level_next_state<=level_next_state;
		L7_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L8_BF;
		    else
			    level_next_state<=level_next_state;
		L8_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L9_BF;
		    else
			    level_next_state<=level_next_state;
		L9_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L10_BF;
		    else
			    level_next_state<=level_next_state;
		L10_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L11_BF;
		    else
			    level_next_state<=level_next_state;
		L11_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L12_BF;
		    else
			    level_next_state<=level_next_state;
		L12_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=L13_BF;
		    else
			    level_next_state<=level_next_state;
		L13_BF:
			if(data_cnt==DATA_NUM)
			    level_next_state<=DATA_OUT;
		    else
			    level_next_state<=level_next_state;
		DATA_OUT:
			if(data_cnt==DATA_NUM)
			    level_next_state<=IDLE;
		    else
			    level_next_state<=level_next_state;
		default:level_next_state<=level_next_state;
		endcase
	end
end


always@(posedge clk)
begin
    if(!rstn) begin
	    //对外状态不允许输入
		din_ready<=0;
		//状态初始化
		bf2_state<=BF_IDLE;
		//数据读写控制
		data_cnt<=0;
		data_ram_wr_en<=0;
		data_ram_rd_en<=0;
		data_ram_addr<=0;
		omega_ram_rd<=0;
		omega_ram_addr<=0;
		bf_din_valid<=0;
		//蝶形循环地址
		group_cycle_cnt<=0;
		interval_1<=0;
		interval_2<=0;
		//运算完成输出标志
		fft_dout_reorder<=0;
		fft_dout_valid<=0;
		fft_dout_last<=0;
	end
	else begin
	    case(level_next_state)
		
		//空闲状态
		IDLE: begin
		    //对外状态允许输入
			din_ready<=1;
			//状态跳转
			bf2_state<=BF_IDLE;
			//数据读写控制
			data_cnt<=0;
			data_ram_wr_en<=0;
			data_ram_rd_en<=0;
			data_ram_addr<=-1;
			omega_ram_rd<=0;
			omega_ram_addr<=0;
			bf_din_valid<=0;
			//蝶形循环地址
		    group_cycle_cnt<=0;
			interval_1<=0;
		    interval_2<=0;
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=data_ram_rd_en;
			fft_dout_last<=data_ram_rd_en;
		end
		
		//读入数据状态
		DATA_IN: begin
		    //对外状态允许输入
			din_ready<=1;
			//状态跳转
			bf2_state<=BF_IDLE;
			//数据读写控制
			if(din_valid_d1) begin
			    data_cnt<=data_cnt+1;
			    data_ram_addr<=data_ram_addr+1;
			    data_ram_wr_en<=1;
			end
			else begin
			    data_cnt<=data_cnt;
			    data_ram_addr<=data_ram_addr;
			    data_ram_wr_en<=0;
			end
			
			data_ram_rd_en<=0;
			omega_ram_rd<=0;
			omega_ram_addr<=0;
			bf_din_valid<=0;
			//蝶形循环地址
		    group_cycle_cnt<=0;
			interval_1<=0;
		    interval_2<=0;
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第一级蝶形运算
		L1_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=4096;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+1;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=1
				//组间距L1_Interval_of_Group=N/2^(m-1)=8192
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=4096
				//组内循环次数L1_Cycle_Count=N/2^m=4096
				if(group_cycle_cnt==4095) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+8192;
					interval_2<=interval_2+8192;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第二级蝶形运算
		L2_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=2048;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+2;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=2
				//组间距L1_Interval_of_Group=N/2^(m-1)=4096
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=2048
				//组内循环次数L1_Cycle_Count=N/2^m=2048
				if(group_cycle_cnt==2047) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+4096;
					interval_2<=interval_2+4096;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第三级蝶形运算
		L3_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=1024;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+4;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=4
				//组间距L1_Interval_of_Group=N/2^(m-1)=2048
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=1024
				//组内循环次数L1_Cycle_Count=N/2^m=1024
				if(group_cycle_cnt==1023) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+2048;
					interval_2<=interval_2+2048;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第四级蝶形运算
		L4_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=512;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+8;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=8
				//组间距L1_Interval_of_Group=N/2^(m-1)=1024
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=512
				//组内循环次数L1_Cycle_Count=N/2^m=512
				if(group_cycle_cnt==511) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+1024;
					interval_2<=interval_2+1024;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第五级蝶形运算
		L5_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=256;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+16;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=16
				//组间距L1_Interval_of_Group=N/2^(m-1)=512
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=256
				//组内循环次数L1_Cycle_Count=N/2^m=256
				if(group_cycle_cnt==255) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+512;
					interval_2<=interval_2+512;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第六级蝶形运算
		L6_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=128;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+32;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=32
				//组间距L1_Interval_of_Group=N/2^(m-1)=256
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=128
				//组内循环次数L1_Cycle_Count=N/2^m=128
				if(group_cycle_cnt==127) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+256;
					interval_2<=interval_2+256;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第七级蝶形运算
		L7_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=64;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+64;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=64
				//组间距L1_Interval_of_Group=N/2^(m-1)=128
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=64
				//组内循环次数L1_Cycle_Count=N/2^m=64
				if(group_cycle_cnt==63) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+128;
					interval_2<=interval_2+128;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第八级蝶形运算
		L8_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=32;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+128;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=128
				//组间距L1_Interval_of_Group=N/2^(m-1)=64
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=32
				//组内循环次数L1_Cycle_Count=N/2^m=32
				if(group_cycle_cnt==31) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+64;
					interval_2<=interval_2+64;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第九级蝶形运算
		L9_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=16;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+256;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=256
				//组间距L1_Interval_of_Group=N/2^(m-1)=32
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=16
				//组内循环次数L1_Cycle_Count=N/2^m=16
				if(group_cycle_cnt==15) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+32;
					interval_2<=interval_2+32;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第十级蝶形运算
		L10_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=8;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+512;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=512
				//组间距L1_Interval_of_Group=N/2^(m-1)=16
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=8
				//组内循环次数L1_Cycle_Count=N/2^m=8
				if(group_cycle_cnt==7) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+16;
					interval_2<=interval_2+16;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第十一级蝶形运算
		L11_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=4;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+1024;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=1024
				//组间距L1_Interval_of_Group=N/2^(m-1)=8
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=4
				//组内循环次数L1_Cycle_Count=N/2^m=4
				if(group_cycle_cnt==3) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+8;
					interval_2<=interval_2+8;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第十二级蝶形运算
		L12_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=2;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+2048;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=2048
				//组间距L1_Interval_of_Group=N/2^(m-1)=4
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=2
				//组内循环次数L1_Cycle_Count=N/2^m=2
				if(group_cycle_cnt==1) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+4;
					interval_2<=interval_2+4;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//第十三级蝶形运算
		L13_BF: begin
			//对外状态允许输入
			din_ready<=0;
			//“级”内循环
			case(bf2_state)
			BF_IDLE: begin
			    //状态跳转
				bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=0;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=0;
				bf_din_valid<=0;
				//蝶形循环地址偏移
		        group_cycle_cnt<=0;
				interval_1<=0;
		        interval_2<=1;
			end
			BF_rd_d1: begin
			    //状态跳转
				bf2_state<=BF_rd_d2;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_rd_d2: begin
			    //状态跳转
			    bf2_state<=BF_js;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=1;
				data_ram_wr_en<=0;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=1;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_js: begin
			    //状态跳转
				bf2_state<=BF_wb_d1;
				//数据读写控制
				data_cnt<=data_cnt;
				data_ram_rd_en<=0;
				data_ram_wr_en<=0;
				data_ram_addr<=0;
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=1;
			end
			BF_wb_d1: begin
			    //状态跳转
				bf2_state<=BF_wb_d2;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_1[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr;
				bf_din_valid<=0;
			end
			BF_wb_d2: begin
			    //状态跳转
				if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_rd_d1;
				//数据读写控制
				data_cnt<=data_cnt+1;
				data_ram_rd_en<=0;
				data_ram_wr_en<=1;
				data_ram_addr<=group_cycle_cnt[12:0]+interval_2[12:0];
				omega_ram_rd<=0;
				omega_ram_addr<=omega_ram_addr+4096;
				bf_din_valid<=0;
				//-----------蝶形循环地址----------
				//组数L1_Num_of_Group=2^(m-1)=4096
				//组间距L1_Interval_of_Group=N/2^(m-1)=2
				//组内蝶形相关运算单元间距L1_Interval_of_Unit=N/2^m=1
				//组内循环次数L1_Cycle_Count=N/2^m=1
				if(group_cycle_cnt==0) begin//完成一个组内循环
				    group_cycle_cnt<=0;
					interval_1<=interval_1+2;
					interval_2<=interval_2+2;
				end
				else begin
				    group_cycle_cnt<=group_cycle_cnt+1;
				end
			end
			default:;
			endcase
			//运算完成输出标志
			fft_dout_reorder<=0;
		    fft_dout_valid<=0;
			fft_dout_last<=0;
		end
		
		//8192点FFT运算完成输出
		DATA_OUT: begin
		    case(bf2_state)
			
			BF_IDLE: begin
			    //对外状态允许输入
			    din_ready<=0;
			    //状态跳转
			    bf2_state<=BF_dout;
			    //数据读写控制
			    data_cnt<=0;
			    data_ram_wr_en<=0;
			    data_ram_rd_en<=0;
			    data_ram_addr<=-1;
			    //运算完成输出标志
		        fft_dout_valid<=0;
			    //旋转因子读写控制
			    omega_ram_rd<=0;
			    omega_ram_addr<=0;
			    bf_din_valid<=0;
			    //蝶形循环地址
		        group_cycle_cnt<=0;
			    interval_1<=0;
		        interval_2<=0;
			end
			BF_dout: begin
			    //对外状态允许输入
			    din_ready<=0;
			    //状态跳转
			    if(data_cnt==8191)
				    bf2_state<=BF_IDLE;
				else
				    bf2_state<=BF_dout;
			    //数据读写控制
			    data_cnt<=data_cnt+1;
			    data_ram_wr_en<=0;
			    data_ram_rd_en<=1;
			    data_ram_addr<=data_ram_addr+1;
			    //运算完成输出标志
				fft_dout_reorder<=1;
		        fft_dout_valid<=data_ram_rd_en;
				fft_dout_last<=0;
			end
			default:;
			endcase
		end
		
		default:;
		endcase
	end
end





endmodule

