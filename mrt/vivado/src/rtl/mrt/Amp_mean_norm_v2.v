`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/08 16:00:21
// Design Name: 
// Module Name: Amp_mean_norm_v2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module Amp_mean_norm_v2#(
     parameter DATA_IN_WIDTH      = 24       ,
	 parameter DATA_OUT_WIDTH     = 24       ,
	 parameter COMPUTE_LEN_WIDTH  = 14    
	 
   )(
    input i_clk,
    input i_rstn,
    input  [DATA_IN_WIDTH-1:0] i_real_data,                       //////fra 22bit
    input i_real_data_vld,
    input i_real_data_last,
    output reg [COMPUTE_LEN_WIDTH-1:0] noweak_count,
    output reg noweak_flag,
    output signed [DATA_IN_WIDTH-1:0] o_real_norm_center,               /////signed 21bit fra
    output o_real_norm_valid,
    output o_real_norm_last
//    output signed  [DATA_IN_WIDTH-1:0] o_sum_r,
//    output o_sum_r_vld
    );
 /////******************************Data path******************************/// 
    reg    [36:0] sum_r;                                        ////////  24+log2(8192)=37
    reg   sum_r_rst;
    reg   Rd_en;
    wire   [DATA_IN_WIDTH-1:0] o_real_data;
    wire  o_real_data_valid;
    reg  sum_r_vld;
    
    wire   [DATA_IN_WIDTH-1:0] o_sum_r;
    reg    [DATA_IN_WIDTH-1:0] Sum_hold;
    wire  o_real_data_last; 
    
    
  /////////////////////// a_mean=mag/N;  
    always @(posedge i_clk) begin
        if(sum_r_rst||~i_rstn) sum_r <= 0;
        else if(i_real_data_vld) sum_r <= sum_r + i_real_data;
        else sum_r <= sum_r;
    end 
    assign o_sum_r = sum_r[36:13];
    
   localparam one = 24'd2097152	 ;	//Q20  
  ////////////////////   anorm=mag/a_mean; 
 wire [47:0] o_real_norm;
 wire [DATA_IN_WIDTH-1:0] o_real_norm_r;
// wire  o_real_norm_valid;
// wire  o_real_norm_last;
 reg  o_real_norm_last_d1;
    
//    div_norm div_norm_inst (
//      .aclk(i_clk),                                      // input wire aclk
//      .s_axis_divisor_tvalid(o_real_data_valid),    // input wire s_axis_divisor_tvalid
//      .s_axis_divisor_tdata(Sum_hold),      // input wire [7 : 0] s_axis_divisor_tdata
//      .s_axis_dividend_tvalid(o_real_data_valid),  // input wire s_axis_dividend_tvalid
//      .s_axis_dividend_tlast(o_real_data_last),    // input wire s_axis_dividend_tlast
//      .s_axis_dividend_tdata(o_real_data),    // input wire [7 : 0] s_axis_dividend_tdata
//      .m_axis_dout_tvalid(o_real_norm_valid),          // output wire m_axis_dout_tvalid
//      .m_axis_dout_tlast(o_real_norm_last),            // output wire m_axis_dout_tlast
//      .m_axis_dout_tdata(o_real_norm)            // [47:24] quo   [23:0]fra
//    );
    divider_myself #(
	     .N          (  48  ) ,
	     .M          (  24  ) ,
	     .N_ACT      (  71  )
	     ) 
    divider_myself_inst (
      .clk(i_clk),                      // input wire clk
      .rstn(i_rstn),
      .data_rdy(o_real_data_valid),      // 
      .dividend({o_real_data,24'b0}),        //
      .divisor(Sum_hold),                  // 
      .res_rdy(o_real_norm_valid),            // 
      .merchant(o_real_norm),           // output wire full
      .remainder() 
    
    );   
    
        xdelay_module #(
	     .D      (  47 ),//////yulai 48 xian 47
	     .W       (   1)
	      )
	    
   xdelay_module_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(o_real_data_last),      
      .s_o(o_real_norm_last) 
    );  

  assign o_real_norm_r={o_real_norm[26:24],21'b0} + {3'b0,o_real_norm[23:3]};  //3bit quo 21bit fra
  
  assign o_real_norm_center=o_real_norm_r-one;  //signed 21bit fra
 ////////////////////////////////////////////   

//      syn_fifo #(
//	     .DATA_WIDTH      (  24 ) ,
//	     .ADDR_WIDTH      (  13  ) ,
//	     .remain_num      (  2  )
//	     ) 
//    syn_fifo_inst (
//      .clk(i_clk),      // input wire clk
//      .rst_n(i_rstn),
//      .wr_data(i_real_data),      // input wire [7 : 0] din
//      .wr_en(i_real_data_vld),  // input wire wr_en
//      .rd_en(Rd_en),  // input wire rd_en
//      .rd_data(o_real_data),    // output wire [7 : 0] dout
//      .full(),    // output wire full
//      .near_full(),  // output wire empty
//      .empty(),
//      .valid(o_real_data_valid)  // output wire valid
//    );
    
   assign o_real_data = Rd_en ? i_real_data :  24'b0;
  assign o_real_data_valid = Rd_en ? i_real_data_vld : 1'b0;

	always @ (posedge i_clk)
            if(i_rstn==1'b0)
		     begin
              o_real_norm_last_d1   <=  'd0 ;
             end	
             else 
		     o_real_norm_last_d1   <= o_real_norm_last;
		     
wire compare_flag;
assign compare_flag=(o_real_norm_r>one)?1:0;
   

  
      always @(posedge i_clk) 
                    begin
                        if(~i_rstn||o_real_norm_last_d1==1)
                            begin
                                noweak_count <= 0   ;
                                noweak_flag <=0    ;
                            end
          //                                     else if (o_real_norm_valid==1)
          //                                      begin
          //                                       noweak_flag <=1 ;     
          //                                     noweak_count <= noweak_count+1  ;
          //                                        end 
             ////                 else if (o_real_norm_valid==1 && compare_flag==1&&o_real_norm_last==1)  此处有修改 为了处理最后一个数大于1的情况
                            
                                else if (o_real_norm_valid==1 && compare_flag==1)
                                begin
                                noweak_count<=noweak_count+1;
                                noweak_flag <=1 ;
                                end 
                                else begin
                                noweak_count<=noweak_count;
                                noweak_flag <=0 ;
                                end 
                        end 
  
  
	always @ (posedge i_clk)
            if(i_rstn==1'b0)
		     begin
              Sum_hold   <=  'd0 ;
             end	
             else if(sum_r_vld) 
              begin  Sum_hold   <= o_sum_r ;
		      end
		      else begin    
		      Sum_hold   <=Sum_hold;
		      end

/////*************************************Control*******************************////      
    reg [1:0] state,next_state;
    reg [COMPUTE_LEN_WIDTH-1:0]  rd_count;
 
  
   localparam Init=2'b00, store=2'b01, Compute=2'b11;
    always @(*) begin
        case(state)
            Init: begin
                if(~i_rstn) next_state = Init;
                else next_state = store;
            end
            store: begin
                if(~i_rstn) next_state = Init;
               else if(i_real_data_last==1) next_state = Compute;
                               else next_state = store;
            end
            Compute: begin
                if(~i_rstn) next_state = Init;
                else next_state = Compute;
            end  
             default:  ;
        endcase
    end
    
    // State transfer block
    always @(posedge i_clk) begin
        if(~i_rstn) state <= Init;
        else state <= next_state;
    end
    

    // State output
    always @(posedge i_clk) begin
        // in default we disable and reset all counter
        sum_r_rst     <= 1;
        sum_r_vld     <= 0; // output data is invalid
        Rd_en         <= 0;
        rd_count      <= 0;
        case(next_state)
            Init: ; // do as the default
            store: begin
                sum_r_vld <= i_real_data_last;
                sum_r_rst <= i_real_data_last;
                Rd_en     <= 0;
                end
            Compute: begin
                sum_r_vld <= i_real_data_last;
                sum_r_rst <= i_real_data_last;
//                Rd_en     <=(i_real_data_last==1||i_real_data_vld==0)?1:0;
//                rd_count  <= ~o_real_data_valid? rd_count: rd_count==15 ? 0:rd_count+1;
                             if(sum_r_vld) 
                                     Rd_en<=1;
                                   else if (rd_count==8191)
                                            Rd_en<=0;
                                             else 
                                            Rd_en<=Rd_en;
 
                              if(Rd_en==1)
                                    rd_count<=rd_count+1;
                                  else
                                   rd_count<=0;          
                end
                 default:  ;
        endcase
    end

 
      assign o_real_data_last = (rd_count==8192);

endmodule