`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/05 23:22:52
// Design Name: 
// Module Name: my_compare
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////





module my_compare #(parameter DATA_BITS = 28)(
    input            i_clk,
    input            i_rstn,
    input   signed   [DATA_BITS-1:0] phase_unwrap,
    output reg signed [DATA_BITS-1:0] phase_unwrap_norm
    );
    
  
  localparam M_2PI = 28'sd13176794	 ;	//Q21
  localparam M_PI =  28'sd6588397   ;
  localparam M_PI_n = -28'sd6588397  ; 
    
 always @(posedge i_clk) 
                begin
                    if(!i_rstn)
                        begin
                            phase_unwrap_norm  <= 0  ;            
                        end
                    else  if  (phase_unwrap < M_PI_n) 
                                            phase_unwrap_norm <= phase_unwrap + M_2PI;    
                                        else if(phase_unwrap > M_PI)
                                            phase_unwrap_norm <= phase_unwrap - M_2PI;
                                        else
                                            phase_unwrap_norm <= phase_unwrap;                                       
                end   
      
      
       endmodule