`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/22 14:22:20
// Design Name: 
// Module Name: Phi_unwrap_nolinear_v3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Phi_unwrap_nolinear_v3#(
     parameter DATA_IN_WIDTH      = 24       ,
	 parameter DATA_OUT_WIDTH     = 8       ,
	 parameter COMPUTE_LEN_WIDTH  = 14    
	 
   )(
    input i_clk,
    input i_rstn,
    input signed [DATA_IN_WIDTH-1:0] i_phase_data,   //1bit 2bit int 21bit fra
    input i_phase_data_vld,
    input i_phase_data_last,
    input noweak_flag,
    input  [COMPUTE_LEN_WIDTH-1:0] noweak_count,
//    output signed  [DATA_IN_WIDTH-1:0] o_sum_r,
//    output o_sum_r_vld
    output intensity_flag,
    output [COMPUTE_LEN_WIDTH-1:0] intensity_count,
    output signed  [DATA_IN_WIDTH-1:0]  o_phase_unwrap_norm_data,
    output  o_phase_unwrap_norm_data_valid,
    output  o_phase_unwrap_norm_data_last
    );
 /////******************************Data path******************************/// 
//  localparam M_2PI = 33'sd3373259426	 ;	//Q29
//  localparam M_PI =  33'sd1686629713   ;
//  localparam M_PI_n = -33'sd1686629713 ;
  
//  localparam M_2PI = 28'sd13176794	 ;	//Q21
//  localparam M_PI =  28'sd6588397   ;
//  localparam M_PI_n = -28'sd6588397  ;
  
    
  localparam M_2PI = 31'sd13176794	 ;	//Q21
  localparam M_PI =  31'sd6588397   ;
  localparam M_PI_n = -31'sd6588397  ;


reg signed [DATA_IN_WIDTH-1:0]   angle_info_d1	,angle_info_d2	;
///////////////////////////////////reg signed [DATA_IN_WIDTH-1:0]   phase_diff		    ;
reg signed [DATA_IN_WIDTH+6:0]   phase_diff	;
reg  i_phase_data_vld_d1,i_phase_data_vld_d2,i_phase_data_vld_d3;
reg  i_phase_data_last_d1,i_phase_data_last_d2,i_phase_data_last_d3;
reg  i_phase_data_vld_pos;

reg signed [DATA_IN_WIDTH+6:0]   phase_crr		        ;  /////////Crr实际的位宽还得经过仿真确定    28bit fra 21bit                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
wire signed [DATA_IN_WIDTH+6:0]   phase_unwrap	        ;   	
wire signed [DATA_IN_WIDTH+6:0]   phase_unwrap_norm	    ;      
wire signed [DATA_IN_WIDTH+6:0]     o_phase_data       ;


always @(posedge i_clk) 
	begin
		if(!i_rstn)
			begin
				angle_info_d1 <= 0	;
				angle_info_d2 <= 0	;
				i_phase_data_vld_d1<=0;
				i_phase_data_vld_d2<=0;
				i_phase_data_vld_d3<=0;
				i_phase_data_last_d1<=0;
				i_phase_data_last_d2<=0;
				i_phase_data_last_d3<=0;
				i_phase_data_vld_pos<=0;
			end
		else 
			begin
				angle_info_d1 <= i_phase_data	   ;
				angle_info_d2 <= angle_info_d1	   ;
				i_phase_data_vld_d1<=i_phase_data_vld;
				i_phase_data_vld_d2<=i_phase_data_vld_d1;
				i_phase_data_vld_d3<=i_phase_data_vld_d2;
				i_phase_data_last_d1<=i_phase_data_last;
				i_phase_data_last_d2<=i_phase_data_last_d1;
				i_phase_data_last_d3<=i_phase_data_last_d2;
				i_phase_data_vld_pos<=i_phase_data_vld&& (~i_phase_data_vld_d1);
			end
	end
	
always @(posedge i_clk) 
	begin
		if(!i_rstn)
			begin
				phase_diff    <= 0  ;			
			end
		else 
			begin
				phase_diff <= i_phase_data - angle_info_d1;
			end
		
	end


 
always @(posedge i_clk) 
        begin
            if(!i_rstn)
                begin
                    phase_crr  <= 0  ;            
                end
            else if(i_phase_data_vld_pos)
                begin
                    phase_crr <= 0;
//                     if  (phase_diff < M_PI_n) 
//                                    phase_crr <= phase_crr + M_2PI;    
//                                else if(phase_diff > M_PI)
//                                    phase_crr <= phase_crr - M_2PI;
//                                else
//                                    phase_crr <= phase_crr;
               end
                               else  if  (phase_diff < M_PI_n) 
                               phase_crr <= phase_crr + M_2PI;    
                           else if(phase_diff > M_PI)
                               phase_crr <= phase_crr - M_2PI;
                           else
                               phase_crr <= phase_crr;
        end
        
        
        assign  phase_unwrap=phase_crr+angle_info_d2;
        
// always @(posedge i_clk) 
//                begin
//                    if(!i_rstn)
//                        begin
//                            phase_unwrap_norm  <= 0  ;            
//                        end
//                    else  if  (phase_unwrap < M_PI_n) 
//                                            phase_unwrap_norm <= phase_unwrap + M_2PI;    
//                                        else if(phase_unwrap > M_PI)
//                                            phase_unwrap_norm <= phase_unwrap - M_2PI;
//                                        else
//                                            phase_unwrap_norm <= phase_unwrap;                                       
//                end       
      cascade_compare #(
        .W              (41),
        .L              (1),
        .DATA_BITS      (31)
    ) cascade_compare11_inst(
        .rstn            (i_rstn),
        .clk            (i_clk),
        .d              (phase_unwrap),
        .out            (phase_unwrap_norm)
    );                 
 
 
 wire   i_phase_data_vld_d9, i_phase_data_last_d9;
    
//   delay_phi_related delay_phi_related_valid (
//  .D(i_phase_data_vld_d3),      // input wire [0 : 0] D
//  .CLK(i_clk),  // input wire CLK
//  .Q(i_phase_data_vld_d9)      // output wire [0 : 0] Q
//);    
       
//      delay_phi_related delay_phi_related_last (
//  .D(i_phase_data_last_d3),      // input wire [0 : 0] D
//  .CLK(i_clk),  // input wire CLK
//  .Q(i_phase_data_last_d9)      // output wire [0 : 0] Q
//);      

   xdelay_module #(
	     .D      (  40),
	     .W       (   1)
	      )
	    
   xdelay_module_last11_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(i_phase_data_last_d3),      
      .s_o(i_phase_data_last_d9) 
    );  

  
   xdelay_module #(
	     .D      (  40),
	     .W       (   1)
	      )
	    
   xdelay_module_valid11_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(i_phase_data_vld_d3),      
      .s_o(i_phase_data_vld_d9) 
    );


//                    integer fp4;
//                    integer i;
//                    initial begin
                 
//                     fp4 = $fopen("phase_unwrap_norm28bitv2.txt");                   
//                     i=0;
//                     end
//                   always @(posedge i_clk) begin
//                      if(i>=8192) begin
//                          $fclose(fp4);
//                     end
//                       else if(i_phase_data_vld_d9) begin
//                          i <= i+1;
//                          $fdisplay(fp4,"%d",phase_unwrap_norm);
//                       end
//                   end              
                   
                   
//                                       integer fp10;
//                    integer j;
//                    initial begin
                 
//                     fp10 = $fopen("phase_unwrap28bitv1.txt");                   
//                     j=0;
//                     end
//                   always @(posedge i_clk) begin
//                      if(j>=8192) begin
//                          $fclose(fp10);
//                     end
//                       else if(i_phase_data_vld_d2) begin
//                          j <= j+1;
//                          $fdisplay(fp10,"%d",phase_unwrap);
//                       end
//                   end  
                   
                   
//                                                         integer fp11;
//                    integer k;
//                    initial begin
                 
//                     fp11 = $fopen("i_phase_data24bitv1.txt");                   
//                     k=0;
//                     end
//                   always @(posedge i_clk) begin
//                      if(k>=8192) begin
//                          $fclose(fp11);
//                     end
//                       else if(i_phase_data_vld) begin
//                          k <= k+1;
//                          $fdisplay(fp11,"%d",i_phase_data);
//                       end
//                   end
/////******************************Data path(求和 减均值。。。。。。)******************************/// 
                    reg  signed  [DATA_IN_WIDTH+19:0] sum_phi;
                    reg   sum_phi_rst;
                    reg   Rd_en;
//                    wire signed  [DATA_IN_WIDTH-1:0] o_real_data;
                    wire  o_phase_data_valid;
                    reg  sum_phi_vld;
                    
                    wire signed   [DATA_IN_WIDTH+6:0] o_sum_phi;
                    reg  signed   [DATA_IN_WIDTH+6:0] Sum_hold;
                    wire  o_phase_data_last; 
                    
                    
                  /////////////////////// m_phi=sum(phase_unwrap_norm)/N;  
                    always @(posedge i_clk) begin
                        if(sum_phi_rst||~i_rstn) sum_phi <= 0;
                        else if(i_phase_data_vld_d9) sum_phi <= sum_phi + phase_unwrap_norm;
                        else sum_phi <= sum_phi;
                    end 
                    assign o_sum_phi = sum_phi[DATA_IN_WIDTH+19:13];
                    
                    
                  ////////////////////   phase_unwrap_norm=phase_unwrap_norm-m_phi
                 wire signed [DATA_IN_WIDTH+6:0] o_phase_unwrap_norm;
               
                    
               assign  o_phase_unwrap_norm=o_phase_data-Sum_hold;
                    
               
                 ////////////////////////////////////////////   
//                    fifo_cache_phi fifo_cache_phi_inst (
//                      .clk(i_clk),      // input wire clk
//                      .din(phase_unwrap_norm),      // input wire [7 : 0] din
//                      .wr_en(i_phase_data_vld_d9),  // input wire wr_en
//                      .rd_en(Rd_en),  // input wire rd_en
//                      .dout(o_phase_data),    // output wire [7 : 0] dout
//                      .full(),    // output wire full
//                      .empty(),  // output wire empty
//                      .valid(o_phase_data_valid)  // output wire valid
//                    );
//    syn_fifo #(
//	     .DATA_WIDTH      (  31 ) ,
//	     .ADDR_WIDTH      (  13 ) ,
//	     .remain_num      (  2  )
//	     ) 
//    fifo_cache_phi_inst (
//      .clk(i_clk),      // input wire clk
//      .rst_n(i_rstn),
//      .wr_data(phase_unwrap_norm),      // input wire [7 : 0] din
//      .wr_en(i_phase_data_vld_d9),  // input wire wr_en
//      .rd_en(Rd_en),  // input wire rd_en
//      .rd_data(o_phase_data),    // output wire [7 : 0] dout
//      .full(),    // output wire full
//      .near_full(),  // output wire empty
//      .empty(),
//      .valid(o_phase_data_valid)  // output wire valid
//    );        
                  assign o_phase_data = Rd_en ? phase_unwrap_norm : 0;
                 assign o_phase_data_valid = Rd_en ? i_phase_data_vld_d9: 1'b0;                                              
                                                       
                  
                    always @ (posedge i_clk)
                            if(i_rstn==1'b0)
                             begin
                              Sum_hold   <=  'd0 ;
                             end    
                             else if(sum_phi_vld) 
                              begin  Sum_hold   <= o_sum_phi ;
                              end
                              else begin    
                              Sum_hold   <=Sum_hold;
                              end
                              
   //////////////////////////////////////同步求幅值模块的非弱信号片段/////////////////////////////////////////////////////
  wire  [DATA_IN_WIDTH+8:0] delay_data;
  
//  delay_phase_data delay_phase_data_inst (
//     .D({o_phase_unwrap_norm,o_phase_data_valid,o_phase_data_last}),      // input wire [13 : 0] D
//     .CLK(i_clk),  // input wire CLK
//     .Q(delay_data)      // output wire [13 : 0] Q
//   ); 
   
      xdelay_module #(
	     .D      (  6 ),
	     .W       (   33)
	      )
	    
   delay_phase_data_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i({o_phase_unwrap_norm,o_phase_data_valid}),      
      .s_o(delay_data[DATA_IN_WIDTH+8:1]) 
    );
   
        xdelay_module #(
	     .D      (  5),
	     .W       (   33)
	      )
	    
   delay_last4_inst (
      .i_clk(i_clk),      // input wire clk
      .i_rstn(i_rstn),
      .s_i(o_phase_data_last),      
      .s_o(delay_data[0]) 
    );
//   assign  o_phase_unwrap_norm_data=delay_data[DATA_IN_WIDTH+1:2];
//////  assign  o_phase_unwrap_norm_data=delay_data[DATA_IN_WIDTH+2:3];
//  assign  o_phase_unwrap_norm_data=delay_data[DATA_IN_WIDTH+8:9];//fra 14bit
 assign  o_phase_unwrap_norm_data=delay_data[DATA_IN_WIDTH+3:4]; //fra 19bit
   assign  o_phase_unwrap_norm_data_valid=delay_data[1];
   assign  o_phase_unwrap_norm_data_last=delay_data[0];
   assign  intensity_flag=noweak_flag;
   assign  intensity_count=noweak_count;
   
   
                     
                   
//                                                         integer fp11;
//                    integer k;
//                    initial begin
                 
//                     fp11 = $fopen("o_phase_unwrap_norm_data24bitv1.txt");                   
//                     k=0;
//                     end
//                   always @(posedge i_clk) begin
//                      if(k>=8192) begin
//                          $fclose(fp11);
//                     end
//                       else if(o_phase_unwrap_norm_data_valid) begin
//                          k <= k+1;
//                          $fdisplay(fp11,"%d",o_phase_unwrap_norm_data);
//                       end
//                   end         
           
           
           
                
                /////*************************************Control*******************************////      
                    reg [1:0] state,next_state;
                    reg [COMPUTE_LEN_WIDTH-1:0]  rd_count;
//                   reg flag;
                  
                   localparam Init=2'b00, store=2'b01, Compute=2'b11;
                    always @(*) begin
                        case(state)
                            Init: begin
                                if(~i_rstn) next_state = Init;
                                else next_state = store;
                            end
                            store: begin
                                if(~i_rstn) next_state = Init;
                               else if(i_phase_data_last_d3==1) next_state = Compute;
                                               else next_state = store;
                            end
                            Compute: begin
                                if(~i_rstn) next_state = Init;
                                else next_state = Compute;
                            end  
                        endcase
                    end
                    
                    // State transfer block
                    always @(posedge i_clk) begin
                        if(~i_rstn) state <= Init;
                        else state <= next_state;
                    end
                    
                
                    // State output
                    always @(posedge i_clk) begin
                        // in default we disable and reset all counter
                        sum_phi_rst     <= 1;
                        sum_phi_vld     <= 0; // output data is invalid
                        Rd_en         <= 0;
                        rd_count      <= 0;
                        case(next_state)
                            Init: ; // do as the default
                            store: begin
                                sum_phi_vld <= i_phase_data_last_d9;
                                sum_phi_rst <= i_phase_data_last_d9;
                                Rd_en     <= 0;
                                end
                            Compute: begin
                                sum_phi_vld <= i_phase_data_last_d9;
                                sum_phi_rst <= i_phase_data_last_d9;
//                                Rd_en     <=(i_phase_data_last_d3==1||i_phase_data_vld_d3==0)?1:0;
//                                rd_count  <= ~o_phase_data_valid? rd_count: rd_count==15 ? 0:rd_count+1;
//                                  Rd_en  <=1;
//                                  rd_count  <=  ~o_phase_data_valid? rd_count: rd_count==15 ? 0:rd_count+1;
                               if(sum_phi_vld) 
                               Rd_en<=1;
                               else if (rd_count==8191)
                                Rd_en<=0;
                                else 
                                Rd_en<=Rd_en;
                                
                                if(Rd_en==1)
                                rd_count<=rd_count+1;
                                else
                                rd_count<=0;
                               
                                end
                                 default:  ;
                        endcase
                    end
                
                 
                      assign o_phase_data_last = (rd_count==8192);

endmodule