`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/09/07 21:57:05
// Design Name: 
// Module Name: Modulation_Recognition_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Modulation_Recognition_top#(
     parameter DATA_IN_WIDTH      = 24     ,

	 parameter DATA_OUT_WIDTH     = 32
	 
   )(
   input                                  i_clk                    ,
   input                                  i_rstn                   ,

 
   input                                  i_Data_In_Valid          ,
   input  [2*DATA_IN_WIDTH-1:0]           i_Data_In                ,
   input                                  i_Data_In_Last           ,
   input                                  work_mode                ,
//   input                                  work_mode_valid          ,

   output   [DATA_OUT_WIDTH-1:0]          u42a_data                , //27bit fra 
   output                                 u42a_data_valid          ,
   output   [DATA_OUT_WIDTH-1:0]          sigma_dp_data            , //23bit fra
   output   [DATA_OUT_WIDTH-1:0]          gama_m_data              , //24bit fra
   output                                 sigma_dp_data_valid      ,
   output                                 gama_m_data_valid        , 
   output  signed [DATA_OUT_WIDTH-1:0]    psy_data                 , //   27bitfra       
   output                                 psy_data_valid
 
    );
    
    
    
    
    wire [DATA_IN_WIDTH-1:0]  o_Real_Data,o_Phase_Data;
    wire                      o_Real_Phase_Valid;
    wire                      o_Real_Phase_Last;
    
    reg  rst_flag,work_mode_reg;
    
//     always @ (posedge i_clk)
//            if(i_rstn==1'b0)
//                work_mode_reg <= 0 ;
//            else if(work_mode_valid)    //0 fast mode  1 pro_mode
//                work_mode_reg<= work_mode;
//                else
//                work_mode_reg<= work_mode_reg;
    
    
    
     always @ (posedge i_clk)
            if(i_rstn==1'b0)
                rst_flag <= 0 ;
            else if(~work_mode)    //0 fast mode  1 pro_mode
                rst_flag<= 0;
                else
                rst_flag<= 1;

    
    
    
    Get_real_phase_v2 #(
	     .DATA_IN_WIDTH      (  DATA_IN_WIDTH  ) ,
	     .COMPUTE_LEN_WIDTH  (  14             ) ,
	     .DATA_OUT_WIDTH     (  DATA_IN_WIDTH  )
	     )
	     Get_real_phase_v2_inst(
            .i_clk(i_clk),
            .i_rstn(i_rstn),
            .i_Data_In_Valid(i_Data_In_Valid),
            .i_Data_In(i_Data_In),
            .i_Data_In_Last(i_Data_In_Last),
            .o_Real_Phase_Valid(o_Real_Phase_Valid),
            .o_Real_Data(o_Real_Data),
            .o_Phase_Data(o_Phase_Data),
            .o_Real_Phase_Last(o_Real_Phase_Last)
        );
        
        wire Notweak_flag;
        wire [13:0] C;
        
        wire [DATA_IN_WIDTH-1:0] o_real_norm_center;
        wire o_real_norm_valid;
        wire  o_real_norm_last;    
        
        
   
   
   
    Amp_mean_norm_v2 #(
	     .DATA_IN_WIDTH      (  DATA_IN_WIDTH  ) ,
	     .COMPUTE_LEN_WIDTH  (  14             ) ,
	     .DATA_OUT_WIDTH     (  DATA_IN_WIDTH  )
	    )
	   Amp_mean_norm_v2_inst(
          .i_clk(i_clk),
          .i_rstn(i_rstn),
          .i_real_data(o_Real_Data),
          .i_real_data_vld(o_Real_Phase_Valid),
          .i_real_data_last(o_Real_Phase_Last),
          .noweak_count(C),
          .noweak_flag(Notweak_flag),
          .o_real_norm_center(o_real_norm_center),
          .o_real_norm_valid(o_real_norm_valid),
          .o_real_norm_last(o_real_norm_last)
                 
             );
             
    
           
        Get_u42a_v2  #(
	     .DATA_IN_WIDTH      (  DATA_IN_WIDTH  ) ,
	     .COMPUTE_LEN_WIDTH  (  14             ) ,
	     .DATA_OUT_WIDTH     (  DATA_OUT_WIDTH  )
	    )   
	        Get_u42a_v2_inst(
            .i_clk(i_clk),
            .i_rstn(i_rstn),
            .i_real_norm_center(o_real_norm_center),
            .i_real_norm_center_vld(o_real_norm_valid),
            .i_real_norm_center_last(o_real_norm_last),
            .u42a_data(u42a_data),
            .u42a_data_valid(u42a_data_valid)
           );  
           
           
           
           
           
        wire intensity_flag;
        wire [13:0] intensity_count;
        wire  o_phase_unwrap_norm_data_valid,  o_phase_unwrap_norm_data_last;
        wire  [DATA_IN_WIDTH-1:0]  o_phase_unwrap_norm_data;
         Phi_unwrap_nolinear_v3  #(
	     .DATA_IN_WIDTH      (  DATA_IN_WIDTH  ) ,
	     .COMPUTE_LEN_WIDTH  (  14             ) ,
	     .DATA_OUT_WIDTH     (  DATA_IN_WIDTH  )
	    )   Phi_unwrap_nolinear_v3_inst(
                 .i_clk(i_clk),
                 .i_rstn(i_rstn),
                 .i_phase_data(o_Phase_Data),
                 .i_phase_data_vld(o_Real_Phase_Valid),
                 .i_phase_data_last(o_Real_Phase_Last),
                 .noweak_count(C),
                 .noweak_flag(Notweak_flag),
                 .intensity_flag(intensity_flag),
                 .intensity_count(intensity_count),
                 .o_phase_unwrap_norm_data(o_phase_unwrap_norm_data),
                 .o_phase_unwrap_norm_data_valid(o_phase_unwrap_norm_data_valid),
                 .o_phase_unwrap_norm_data_last(o_phase_unwrap_norm_data_last)
                 
                 
             );    
             
  
         Get_sigma_dp_v3 #(
	     .DATA_IN_WIDTH      (  DATA_IN_WIDTH  ) ,
	     .COMPUTE_LEN_WIDTH  (  14             ) ,
	     .DATA_OUT_WIDTH     (  DATA_OUT_WIDTH  )
	     )  Get_sigma_dp_v3_inst(
            .i_clk(i_clk),
            .i_rstn(i_rstn),
            .i_intensity_flag(intensity_flag),
            .i_intensity_count(intensity_count),
            .i_phase_data(o_phase_unwrap_norm_data),
            .i_phase_data_valid(o_phase_unwrap_norm_data_valid),
            .i_phase_data_last(o_phase_unwrap_norm_data_last),
            .sigma_dp_data(sigma_dp_data),
            .sigma_dp_data_valid(sigma_dp_data_valid)

           );   
           
           
 
           
        Get_gama_m_v2#(
	     .DATA_IN_WIDTH      (  DATA_IN_WIDTH  ) ,
	     .COMPUTE_LEN_WIDTH  (  14             ) ,
	     .DATA_OUT_WIDTH     (  DATA_OUT_WIDTH  )
	     )  Get_gama_m_v2_inst(
            .i_clk(i_clk),
            .i_rstn(i_rstn&&rst_flag),
            .i_real_norm_center(o_real_norm_center),
            .i_real_norm_center_vld(o_real_norm_valid),
            .i_real_norm_center_last(o_real_norm_last),
            .gama_m_vld(gama_m_data_valid),
            .gama_m(gama_m_data)
           );
           
          
          
         Get_p#(
	     .DATA_IN_WIDTH      (  DATA_IN_WIDTH  ) ,
	     .COMPUTE_LEN_WIDTH  (  14             )
	    )  Get_p_inst(
            .i_clk(i_clk),
            .i_rstn(i_rstn&&rst_flag),
            .i_Data_In(i_Data_In),
            .i_Data_In_Valid(i_Data_In_Valid),
            .i_Data_In_Last(i_Data_In_Last),
            .psy_data(psy_data),
            .psy_data_valid(psy_data_valid)
           );  
           endmodule