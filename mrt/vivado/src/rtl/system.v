`timescale 1ns/1ps

module system
(
  input wire CLK200MHZ_n,//GCLK-W19
  input wire CLK200MHZ_p,//GCLK-W19
  input wire mcu_rst,//MCU_RESET-P20
                           
  //gpioA
  inout wire [31:0] gpioA,//GPIOA00~GPIOA31
  //pmu_wakeup

  inout wire pmu_paden,  //PMU_VDDPADEN-U15
  inout wire pmu_padrst, //PMU_VADDPARST_V15
  inout wire mcu_wakeup,  //MCU_WAKE-N15
  
    input              touch_key   , //触摸按键,用于触发开发板发出ARP请求
    //PL以太网SGMII接口   
    input              ref_clk_n   ,
    input              ref_clk_p   ,
    output             eth_rst_n   ,
    input              eth_rx_n    ,
    input              eth_rx_p    ,
    output             eth_tx_n    ,
    output             eth_tx_p    ,
    //MDIO接口
    output             eth_mdc     , //PHY管理接口的时钟信号
    inout              eth_mdio      //PHY管理接口的双向数据信号
  
);


      // adc enframe block interface
    wire                         u_mrt_wen           ;
    wire     [24*2-1: 0]         u_mrt_cfg_data      ;
    wire                         u_mrt_cfg_last      ;


  wire mmcm_locked;
  wire  CLK32768KHZ;
  reg [6:0] CLK32768KHZ_cnt;
  wire reset_periph;

  wire ck_rst;

  // All wires connected to the chip top
  wire dut_clock;
  wire dut_reset;

  wire dut_io_pads_jtag_TCK_i_ival = 1'b0;
  wire dut_io_pads_jtag_TMS_i_ival = 1'b1;
  wire dut_io_pads_jtag_TMS_o_oval;
  wire dut_io_pads_jtag_TMS_o_oe;
  wire dut_io_pads_jtag_TMS_o_ie;
  wire dut_io_pads_jtag_TMS_o_pue;
  wire dut_io_pads_jtag_TMS_o_ds;
  wire dut_io_pads_jtag_TDI_i_ival = 1'b1;
  wire dut_io_pads_jtag_TDO_o_oval;
  wire dut_io_pads_jtag_TDO_o_oe;

  wire [32-1:0] dut_io_pads_gpioA_i_ival;
  wire [32-1:0] dut_io_pads_gpioA_o_oval;
  wire [32-1:0] dut_io_pads_gpioA_o_oe;

//  wire [32-1:0] dut_io_pads_gpioB_i_ival;
  wire [32-1:0] dut_io_pads_gpioB_o_oval;
//  wire [32-1:0] dut_io_pads_gpioB_o_oe;

//  wire dut_io_pads_qspi0_sck_o_oval;
//  wire dut_io_pads_qspi0_cs_0_o_oval;
//  wire dut_io_pads_qspi0_dq_0_i_ival;
//  wire dut_io_pads_qspi0_dq_0_o_oval;
//  wire dut_io_pads_qspi0_dq_0_o_oe;
//  wire dut_io_pads_qspi0_dq_1_i_ival;
//  wire dut_io_pads_qspi0_dq_1_o_oval;
//  wire dut_io_pads_qspi0_dq_1_o_oe;
//  wire dut_io_pads_qspi0_dq_2_i_ival;
//  wire dut_io_pads_qspi0_dq_2_o_oval;
//  wire dut_io_pads_qspi0_dq_2_o_oe;
//  wire dut_io_pads_qspi0_dq_3_i_ival;
//  wire dut_io_pads_qspi0_dq_3_o_oval;
//  wire dut_io_pads_qspi0_dq_3_o_oe;


  wire dut_io_pads_aon_erst_n_i_ival;
  wire dut_io_pads_aon_pmu_dwakeup_n_i_ival;
  wire dut_io_pads_aon_pmu_vddpaden_o_oval;
  wire dut_io_pads_aon_pmu_padrst_o_oval ;
  wire dut_io_pads_bootrom_n_i_ival;
  wire dut_io_pads_dbgmode0_n_i_ival;
  wire dut_io_pads_dbgmode1_n_i_ival;
  wire dut_io_pads_dbgmode2_n_i_ival;

  //=================================================
  // Clock & Reset
  wire clk_8388;
  wire clk_16M;
  wire hfclk;
  wire clk_ila;
  wire uclk200;
  
IBUFDS uu_uclk200 (
.O (uclk200),
.I (CLK200MHZ_p),
.IB (CLK200MHZ_n));
    
  mmcm ip_mmcm
  (
    .resetn(1'b1),
    .clk_in1(uclk200),
    .clk_out1(clk_8388), // 8.388MHz, this frequency can be divided by 256 to generate a 32.768kHz RTC clock  
    .clk_out2(clk_16M), // 16 MHz, this clock we set to 16MHz 
    .clk_out3(clk_ila),
    .locked(mmcm_locked)
  );

    
  reg CLK32768KHZ_ibufg;

  assign ck_rst =  ~mcu_rst;

  always@(posedge clk_8388) begin
    if(~ck_rst) begin 
      CLK32768KHZ_cnt <= 7'b0;
      CLK32768KHZ_ibufg <= 1'b0;
    end
    else begin 
      CLK32768KHZ_cnt <= CLK32768KHZ_cnt + 1;
      if(CLK32768KHZ_cnt == 7'h7f) CLK32768KHZ_ibufg <= ~CLK32768KHZ_ibufg;
//      if(CLK32768KHZ_cnt[0] == 1'b1) CLK32768KHZ_ibufg <= ~CLK32768KHZ_ibufg;
    end
  end

  
  BUFG u_rtcclk_bufg (
    .I(CLK32768KHZ_ibufg),
    .O(CLK32768KHZ)
  );

  reset_sys ip_reset_sys
  (
    .slowest_sync_clk(clk_16M),
    .ext_reset_in(ck_rst), // Active-low
    .aux_reset_in(1'b1),
    .mb_debug_sys_rst(1'b0),
    .dcm_locked(mmcm_locked),
    .mb_reset(),
    .bus_struct_reset(),
    .peripheral_reset(reset_periph),
    .interconnect_aresetn(),
    .peripheral_aresetn()
  );

  IOBUF
  #(
    .DRIVE(12),
    .IBUF_LOW_PWR("TRUE"),
    .IOSTANDARD("DEFAULT"),
    .SLEW("SLOW")
  )
  gpioA_iobuf[31:0]
  (
    .O(dut_io_pads_gpioA_i_ival),
    .IO(gpioA),
    .I(dut_io_pads_gpioA_o_oval),
    .T(~dut_io_pads_gpioA_o_oe)
  );


  //=================================================
  // Assignment of IOBUF "IO" pins to package pins

  // Pins IO0-IO13
  // Shield header row 0: PD0-PD7

  // Use the LEDs for some more useful debugging things.
  assign pmu_paden  = dut_io_pads_aon_pmu_vddpaden_o_oval;  
  assign pmu_padrst = dut_io_pads_aon_pmu_padrst_o_oval;		

  // model select
  assign dut_io_pads_bootrom_n_i_ival  = 1'b0;   // boot from ROM
  assign dut_io_pads_dbgmode0_n_i_ival = 1'b1;
  assign dut_io_pads_dbgmode1_n_i_ival = 1'b1;
  assign dut_io_pads_dbgmode2_n_i_ival = 1'b1;
  //

  e203_soc_top dut
  (
    .hfextclk(clk_16M),
    .hfxoscen(),
    .hfclk(hfclk),
    .lfextclk(CLK32768KHZ), 
    .lfxoscen(),

   // Note: this is the real SoC top AON domain slow clock
    .io_pads_jtag_TCK_i_ival (),
    .io_pads_jtag_TMS_i_ival (),
    .io_pads_jtag_TDI_i_ival (),
    .io_pads_jtag_TDO_o_oval (),
    .io_pads_jtag_TDO_o_oe  (),

    .io_pads_gpioA_i_ival(dut_io_pads_gpioA_i_ival),
    .io_pads_gpioA_o_oval(dut_io_pads_gpioA_o_oval),
    .io_pads_gpioA_o_oe  (dut_io_pads_gpioA_o_oe),

    .io_pads_gpioB_i_ival(32'b0),
    .io_pads_gpioB_o_oval(dut_io_pads_gpioB_o_oval),
    .io_pads_gpioB_o_oe  ( ),

   .io_pads_qspi0_sck_o_oval (),
   .io_pads_qspi0_cs_0_o_oval(),
   .io_pads_qspi0_dq_0_i_ival(1'b1),
   .io_pads_qspi0_dq_0_o_oval(),
   .io_pads_qspi0_dq_0_o_oe  (),
   .io_pads_qspi0_dq_1_i_ival(1'b1),
   .io_pads_qspi0_dq_1_o_oval(),
   .io_pads_qspi0_dq_1_o_oe  (),
   .io_pads_qspi0_dq_2_i_ival(1'b1),
   .io_pads_qspi0_dq_2_o_oval(),
   .io_pads_qspi0_dq_2_o_oe  (),
   .io_pads_qspi0_dq_3_i_ival(1'b1),
   .io_pads_qspi0_dq_3_o_oval(),
   .io_pads_qspi0_dq_3_o_oe  (),


       // Note: this is the real SoC top level reset signal
    .io_pads_aon_erst_n_i_ival(ck_rst),
    .io_pads_aon_pmu_dwakeup_n_i_ival(dut_io_pads_aon_pmu_dwakeup_n_i_ival),
    .io_pads_aon_pmu_vddpaden_o_oval(dut_io_pads_aon_pmu_vddpaden_o_oval),

    .io_pads_aon_pmu_padrst_o_oval    (dut_io_pads_aon_pmu_padrst_o_oval ),

    .io_pads_bootrom_n_i_ival       (dut_io_pads_bootrom_n_i_ival),

    .io_pads_dbgmode0_n_i_ival       (dut_io_pads_dbgmode0_n_i_ival),
    .io_pads_dbgmode1_n_i_ival       (dut_io_pads_dbgmode1_n_i_ival),
    .io_pads_dbgmode2_n_i_ival       (dut_io_pads_dbgmode2_n_i_ival) ,
    
    // adc enframe block interface
        .u_mrt_wen            (u_mrt_wen),
        .u_mrt_cfg_data       (u_mrt_cfg_data),
        .u_mrt_cfg_last       (u_mrt_cfg_last),
        
    .t_nice_core_isolate (1'b0),
    .t_nice_core_cfg     (12'd0),
    .t_nice_core_res     ()
  );
  
//  `define SUBSYS_MAIN dut.u_e203_subsys_top.u_e203_subsys_main
//  `define PERIPH SUBSYS_MAIN.u_e203_subsys_perips
//  `define CPU SUBSYS_MAIN.u_e203_cpu_top.u_e203_cpu
//  `define IFU CPU.u_e203_core.u_e203_ifu.u_e203_ifu_ifetch
  
//  ila_0 u_ila(
// .clk(clk_ila),
//// .probe0(dut_io_pads_jtag_TCK_i_ival),
//// .probe1(dut_io_pads_jtag_TMS_i_ival),
//// .probe2(dut_io_pads_jtag_TDI_i_ival),
//// .probe3(dut_io_pads_jtag_TDO_o_oval),
//// .probe0(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.nice_clk),
//// .probe1(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.u_e203_nice_mrt_core.mrt_rstn),//24
//// .probe2(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.u_e203_nice_mrt_core.u42a_data_valid),
//// .probe3(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.u_e203_nice_mrt_core.nice_req_valid),//14
//// .probe4(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.u_e203_nice_mrt_core.workmode_status_r),
//// .probe5(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.u_e203_nice_mrt_core.state_is_start),
//// .probe6(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.u_e203_nice_mrt_core.busy_status_r),
//// .probe7(u_mrt_cfg_data[31:0]),//32
//// .probe8(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.u_e203_nice_mrt_core.gamma_data_valid),
//// .probe9(dut.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu.u_e203_nice_icb_bridge.nice_req_inst)//32

//);
  // Assign reasonable values to otherwise unconnected inputs to chip top

  wire iobuf_dwakeup_o;
  IOBUF
  #(
    .DRIVE(12),
    .IBUF_LOW_PWR("TRUE"),
    .IOSTANDARD("DEFAULT"),
    .SLEW("SLOW")
  )
  IOBUF_dwakeup_n
  (
    .O(iobuf_dwakeup_o),
    .IO(mcu_wakeup),
    .I(1'b1),
    .T(1'b1)
  );
  assign dut_io_pads_aon_pmu_dwakeup_n_i_ival = (~iobuf_dwakeup_o);

    mrt_input_simulator u_adc_sim(
    
        .i_clk(clk_16M),
        .i_rstn(ck_rst),
        .i_ena(dut_io_pads_gpioB_o_oval[4]),
        .o_data(u_mrt_cfg_data),
        .o_data_valid(u_mrt_wen),
        .o_data_last(u_mrt_cfg_last),
        .signal_type(dut_io_pads_gpioB_o_oval[3:0]),
    
    // ethernet interface
    .uclk200     (uclk200), //系统时钟
    .touch_key   (touch_key), //触摸按键,用于触发开发板发出ARP请求
    //PL以太网SGMII接口   
    .ref_clk_n   (ref_clk_n),
    .ref_clk_p   (ref_clk_p),
    .eth_rst_n   (eth_rst_n),
    .eth_rx_n    (eth_rx_n),
    .eth_rx_p    (eth_rx_p),
    .eth_tx_n    (eth_tx_n),
    .eth_tx_p    (eth_tx_p),
    //MDIO接口
    .eth_mdc     (eth_mdc), //PHY管理接口的时钟信号
    .eth_mdio    (eth_mdio) //PHY管理接口的双向数据信号
        );

endmodule


