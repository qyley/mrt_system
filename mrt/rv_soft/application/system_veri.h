#ifndef __SYSTEM_VERI_H__
#define __SYSTEM_VERI_H__

extern unsigned int global_sigma;
extern unsigned int global_u42a;
extern unsigned int global_gamma;
extern unsigned int global_psy;
extern unsigned int mrt_cfg_wm;
extern unsigned int mrt_cfg_on;
extern unsigned int mrt_cfg_ur;
extern unsigned int mrt_finish_flag;
extern unsigned int mrt_type;

#define SOC_ADC_ENA_GPIO_OFS   		4
#define SOC_ADC_TYPE3_GPIO_OFS 		3
#define SOC_ADC_TYPE2_GPIO_OFS  	2
#define SOC_ADC_TYPE1_GPIO_OFS   	1
#define SOC_ADC_TYPE0_GPIO_OFS   	0
#define SOC_MRT_INTR_GPIO_OFS   	31

#define SOC_SWC0_GPIO_OFS   		31
#define SOC_SWC1_GPIO_OFS 			30
#define SOC_SWC2_GPIO_OFS  			29
#define SOC_SWC3_GPIO_OFS   		28
#define SOC_SWC4_GPIO_OFS   		27
#define SOC_SWC5_GPIO_OFS   		26

#define SOC_LED0_GPIO_OFS   		25
#define SOC_LED1_GPIO_OFS 			24
#define SOC_LED2_GPIO_OFS  			23
#define SOC_LED3_GPIO_OFS   		22
#define SOC_LED4_GPIO_OFS   		21
#define SOC_LED5_GPIO_OFS   		20

#define SOC_ADC_ENA_GPIO_MSK   		(1<<SOC_ADC_ENA_GPIO_OFS)
#define SOC_ADC_TYPE3_GPIO_MSK  	(1<<SOC_ADC_TYPE3_GPIO_OFS)
#define SOC_ADC_TYPE2_GPIO_MSK  	(1<<SOC_ADC_TYPE2_GPIO_OFS)
#define SOC_ADC_TYPE1_GPIO_MSK   	(1<<SOC_ADC_TYPE1_GPIO_OFS)
#define SOC_ADC_TYPE0_GPIO_MSK   	(1<<SOC_ADC_TYPE0_GPIO_OFS)
#define SOC_MRT_INTR_GPIO_MSK   	(1<<SOC_MRT_INTR_GPIO_OFS)

#define SOC_SWC0_GPIO_MSK   		1<<31
#define SOC_SWC1_GPIO_MSK 			1<<30
#define SOC_SWC2_GPIO_MSK  			1<<29
#define SOC_SWC3_GPIO_MSK   		1<<28
#define SOC_SWC4_GPIO_MSK   		1<<27
#define SOC_SWC5_GPIO_MSK   		1<<26

#define SOC_LED0_GPIO_MSK   		1<<25
#define SOC_LED1_GPIO_MSK 			1<<24
#define SOC_LED2_GPIO_MSK  			1<<23
#define SOC_LED3_GPIO_MSK   		1<<22
#define SOC_LED4_GPIO_MSK   		1<<21
#define SOC_LED5_GPIO_MSK   		1<<20

#define SOC_ADC_CTRL_GPIO_MSK 	(SOC_ADC_TYPE3_GPIO_MSK|SOC_ADC_TYPE2_GPIO_MSK|SOC_ADC_TYPE1_GPIO_MSK|SOC_ADC_TYPE0_GPIO_MSK)
#define SOC_MODESEL_GPIO_MSK	(SOC_SWC0_GPIO_MSK|SOC_SWC1_GPIO_MSK|SOC_SWC2_GPIO_MSK|SOC_SWC3_GPIO_MSK|SOC_SWC4_GPIO_MSK|SOC_SWC5_GPIO_MSK)
#define SOC_SYSSTATUS_GPIO_MSK	(SOC_LED0_GPIO_MSK|SOC_LED1_GPIO_MSK)
#define SOC_MRTRESULT_GPIO_MSK	(SOC_LED2_GPIO_MSK|SOC_LED3_GPIO_MSK|SOC_LED4_GPIO_MSK|SOC_LED5_GPIO_MSK)

void gpio_init();
void config();
void adc_init();
void adc_select_type_and_start(int i);
void adc_stop();
void mrt_intr_init();
void print_fixed_point(unsigned int fp_num, int bw, int frac_bw, int usign, int pricision);
#endif
