#ifndef __STARTUP_H__
#define __STARTUP__

#include <hbird_sdk_soc.h>

// function do everytime system startup
void print_misa(void);
void uart_programming_entry(void);
void uart_programming(void);
//void uart_programming_binary(void);

#endif
