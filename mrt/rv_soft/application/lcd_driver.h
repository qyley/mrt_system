#ifndef __LCD_DRIVER_H__
#define __LCD_DRIVER_H__

#include "hbird_sdk_soc.h"

#define SOC_LCD_E_GPIO_OFS   	9
#define SOC_LCD_RS_GPIO_OFS 	10
#define SOC_LCD_RW_GPIO_OFS  	11
#define SOC_LCD_DB4_GPIO_OFS   	12
#define SOC_LCD_DB5_GPIO_OFS   	13
#define SOC_LCD_DB6_GPIO_OFS   	14
#define SOC_LCD_DB7_GPIO_OFS   	15

#define SOC_LCD_E_GPIO_MSK   	(1<<SOC_LCD_E_GPIO_OFS)
#define SOC_LCD_RS_GPIO_MSK  	(1<<SOC_LCD_RS_GPIO_OFS)
#define SOC_LCD_RW_GPIO_MSK  	(1<<SOC_LCD_RW_GPIO_OFS)
#define SOC_LCD_DB7_GPIO_MSK   	(1<<SOC_LCD_DB7_GPIO_OFS)
#define SOC_LCD_DB6_GPIO_MSK   	(1<<SOC_LCD_DB6_GPIO_OFS)
#define SOC_LCD_DB5_GPIO_MSK   	(1<<SOC_LCD_DB5_GPIO_OFS)
#define SOC_LCD_DB4_GPIO_MSK   	(1<<SOC_LCD_DB4_GPIO_OFS)

#define SOC_LCD_DB_GPIO_MSK 	(SOC_LCD_DB4_GPIO_MSK|SOC_LCD_DB5_GPIO_MSK|SOC_LCD_DB6_GPIO_MSK|SOC_LCD_DB7_GPIO_MSK)

#define SOC_LCD_CTRL_GPIO_MSK 	(SOC_LCD_RW_GPIO_MSK|SOC_LCD_RS_GPIO_MSK|SOC_LCD_E_GPIO_MSK)


void lcd_init();
void lcd_clear();
void lcd_print(int len, char str[],int row, int col);

#endif
