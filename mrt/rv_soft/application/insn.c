#include "insn.h"
#include <stdio.h>

int mrt_read_status(){
	return custom_mrt_rdstat(0);
}

int mrt_is_busy(){
	volatile unsigned int status;
	status = mrt_read_status();
	return status & IS_BUSY_MSK;
}

int mrt_is_u42a_rdy(){
	volatile unsigned int status;
	status = mrt_read_status();
	return status & U42A_RDY_MSK;
}

int mrt_is_sigma_rdy(){
	volatile unsigned int status;
	status = mrt_read_status();
	return status & SIGMA_RDY_MSK;
}

int mrt_start(){
	return custom_mrt_start();
}

int mrt_read_u42a(){
	return custom_mrt_rdstat(1);
}

int mrt_read_sigma(){
	return custom_mrt_rdstat(2);
}

// Deprecated function
//int mrt_load_buffer(int data[],int start_addr, int batch_size, int cs){
//	volatile unsigned int cfgbits;
//	cfgbits = ((start_addr<<8) & LDBUF_SA_MSK) | ((batch_size<<1) & LDBUF_BS_MSK) | ((cs) & LDBUF_CS_MSK);
//	return custom_mrt_ldbuf((int)data, cfgbits);
//}

int prg_read_status(){
	return custom_prg_rdstat(0);
}

int prg_is_busy(){
	volatile unsigned int status;
	status = prg_read_status();
	return status & IS_BUSY_MSK;
}

// Deprecated function
//int prg_load_buffer(int data[],int start_addr){
//	volatile unsigned int cfgbits;
//	cfgbits = (start_addr<<8) & LDBUF_SA_MSK;
//	return custom_prg_ldbuf((int)data, cfgbits);
//}

int prg_start(int itcm[]){
    return custom_prg_start(0x80000000, (int)itcm);
}
