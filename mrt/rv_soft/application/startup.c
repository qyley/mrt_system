#include "startup.h"
#include "insn.h"
#include <stdio.h>


void print_misa(void)
{
    CSR_MISA_Type misa_bits = (CSR_MISA_Type) __RV_CSR_READ(CSR_MISA);
    static char misa_chars[30];
    uint8_t index = 0;
    if (misa_bits.b.mxl == 1) {
        misa_chars[index++] = '3';
        misa_chars[index++] = '2';
    } else if (misa_bits.b.mxl == 2) {
        misa_chars[index++] = '6';
        misa_chars[index++] = '4';
    } else if (misa_bits.b.mxl == 3) {
        misa_chars[index++] = '1';
        misa_chars[index++] = '2';
        misa_chars[index++] = '8';
    }
    if (misa_bits.b.i) {
        misa_chars[index++] = 'I';
    }
    if (misa_bits.b.m) {
        misa_chars[index++] = 'M';
    }
    if (misa_bits.b.a) {
        misa_chars[index++] = 'A';
    }
    if (misa_bits.b.b) {
        misa_chars[index++] = 'B';
    }
    if (misa_bits.b.c) {
        misa_chars[index++] = 'C';
    }
    if (misa_bits.b.e) {
        misa_chars[index++] = 'E';
    }
    if (misa_bits.b.f) {
        misa_chars[index++] = 'F';
    }
    if (misa_bits.b.d) {
        misa_chars[index++] = 'D';
    }
    if (misa_bits.b.q) {
        misa_chars[index++] = 'Q';
    }
    if (misa_bits.b.h) {
        misa_chars[index++] = 'H';
    }
    if (misa_bits.b.j) {
        misa_chars[index++] = 'J';
    }
    if (misa_bits.b.l) {
        misa_chars[index++] = 'L';
    }
    if (misa_bits.b.n) {
        misa_chars[index++] = 'N';
    }
    if (misa_bits.b.s) {
        misa_chars[index++] = 'S';
    }
    if (misa_bits.b.p) {
        misa_chars[index++] = 'P';
    }
    if (misa_bits.b.t) {
        misa_chars[index++] = 'T';
    }
    if (misa_bits.b.u) {
        misa_chars[index++] = 'U';
    }
    if (misa_bits.b.x) {
        misa_chars[index++] = 'X';
    }

    misa_chars[index++] = '\0';

    printf("MISA: RV%s\r\n", misa_chars);
}


void uart_programming_entry(void){
	char str[50];
	printf("need program ? y/n \n");
	while(!scanf("%s",str));
	if(str[0] == 'y'){
		uart_programming();
		return ;
	}
//	else if(str[0] == 'b'){
//		uart_programming_binary();
//		return ;
//	}
	else{
		printf("start main. \n");
		return ;
	}
}


//void uart_programming_binary(void){
//	unsigned int itcm_buf[12*1024]; 	    // can store 48kB ITCM Data
//	int lor; 						// length of row (no more than 12288)
//	int i;
//	unsigned int c1,c2,c3,c4;
//	if(prg_is_busy()){
//		printf("program download fail. prg core busy.\r\n");
//	}
//	else {
//		printf("enter itcm num of bytes (no more than 49152 bytes): \r\n");
//		while(!scanf("%d",&lor));
//		lor = lor / 4;
//		i = 0;
//		printf("bytes = %d, select itcm binary file: \r\n",lor);
//		while(i<lor){
//			if(scanf("%c%c%c%c",c4,c3,c2,c1)){
//				itcm_buf[i] = c4<<24 | c3<<16 | c2<<8 | c1;
//				if(i%1000==0) printf("%d: %u.\r\n", i, itcm_buf[i]);
//				i = i+1;
//			}
//		}
//		printf("programming. please reboot the RISC-V after a while.\r\n");
//		while(!scanf("%d",&lor));
//		delay_1ms(50);
//		prg_start(itcm_buf);
//	}
//	return ;
//}


void uart_programming(void){
	unsigned int itcm_buf[12*1024]; 	// can store 48kB ITCM Data
	int lor; 						// length of row (no more than 12288)
	int i;
	if(prg_is_busy()){
		printf("program download fail. prg core busy.\r\n");
	}
	else {
		printf("enter itcm rows (no more than 12288): \r\n");
		while(!scanf("%d",&lor));
		i = 0;
		printf("rows = %d, select itcm file: \r\n",lor);
		while(i<lor){
			if(scanf("%u",(itcm_buf+i))){
				if(i%100==0) printf("%d: %u.\r\n", i, itcm_buf[i]);
				i = i+1;
			}
		}
		printf("programming. please reboot the RISC-V after a while.\r\n");
		prg_start(itcm_buf);
	}
	return ;
}
