################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/insn.c \
../application/lcd_driver.c \
../application/main.c \
../application/startup.c \
../application/system_veri.c 

OBJS += \
./application/insn.o \
./application/lcd_driver.o \
./application/main.o \
./application/startup.o \
./application/system_veri.o 

C_DEPS += \
./application/insn.d \
./application/lcd_driver.d \
./application/main.d \
./application/startup.d \
./application/system_veri.d 


# Each subdirectory must supply rules for building sources it contributes
application/%.o: ../application/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv-nuclei-elf-gcc -march=rv32imac -mabi=ilp32 -mcmodel=medany -mno-save-restore -O0 -ffunction-sections -fdata-sections -fno-common  -g -DDOWNLOAD_MODE=DOWNLOAD_MODE_ILM -DSOC_HBIRDV2 -DBOARD_DDR200T -I"D:\table\Project\VC707_mrt\system_mrt_soft\application" -I"D:\table\Project\VC707_mrt\system_mrt_soft\hbird_sdk\NMSIS\Core\Include" -I"D:\table\Project\VC707_mrt\system_mrt_soft\hbird_sdk\SoC\hbirdv2\Common\Include" -I"D:\table\Project\VC707_mrt\system_mrt_soft\hbird_sdk\SoC\hbirdv2\Board\ddr200t\Include" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


