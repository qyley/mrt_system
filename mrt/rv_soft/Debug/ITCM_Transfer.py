#coding=utf-8
'''
Transfer 8bit itcm file to 64bit or other
'''
import os, re
import struct

ITCM_DATAWIDTH       = 32
WITH_DEC             = 1
WITH_BIN             = 0
ITCM_DATAWIDTH_BYTE  = int(ITCM_DATAWIDTH/8)
ITCM_BYTE_SIZE       = 65536
ITCM_DEPTH           = int(ITCM_BYTE_SIZE/ITCM_DATAWIDTH_BYTE)
FILE_NAME            = "system_mrt_soft.verilog"

def transfer_func(filename):
    ''' functional '''
    
    itcmdata = [["0" for j in range(ITCM_DATAWIDTH_BYTE)] for i in range(ITCM_DEPTH)]
        
    with open(filename) as f:
        data = f.readlines()
        print(len(data))
        l = len(data)
        row = 0
        reg_byte_addr = 0
        
        while row < l:
            linedata = data[row]
            if linedata[0] == '@': # address location statement
                reg_byte_addr = int(linedata[1:-1],16)
                row = row + 1
            else: # data statement
                for databyte in re.split(r" ",linedata):
                    if databyte is not '\n':
                        data_ind = int(reg_byte_addr/ITCM_DATAWIDTH_BYTE)
                        data_byte_loc = ITCM_DATAWIDTH_BYTE - (reg_byte_addr - data_ind*ITCM_DATAWIDTH_BYTE) - 1
                        #print(data_ind)
                        #print(data_byte_loc)
                        itcmdata[data_ind][data_byte_loc] = databyte
                        reg_byte_addr = reg_byte_addr + 1
                row = row + 1
                print(row)
            #print(itcmdata[row-1])


    finfo = re.split(r"\.",filename)
    if WITH_DEC:
        savefile = "{}_{}_dec.txt".format(finfo[0],ITCM_DATAWIDTH)
    elif WITH_BIN:
        savefile = "{}_{}_bin.bin".format(finfo[0],ITCM_DATAWIDTH)
    else:
        savefile = "{}_{}.{}".format(finfo[0],ITCM_DATAWIDTH,finfo[1])
    fsave = open(savefile, "wb")
    for row in range(data_ind + 1):
        dataneedwrite = "".join(itcmdata[row])
        if WITH_DEC:
            dataneedwrite = int(dataneedwrite,16)
            dataneedwrite = "{}".format(dataneedwrite)
            fsave.write(dataneedwrite.encode())
            fsave.write('\n'.encode())
        elif WITH_BIN:
            dataneedwrite = int(dataneedwrite,16)
            try: dataneedwrite = struct.pack('B',dataneedwrite)
            except: print('ITCM DATAWIDTH MUST = 8')
            fsave.write(dataneedwrite)
        else:
            fsave.write(dataneedwrite.encode())
            fsave.write('\n'.encode())
    f.close()
    print('finish.')
    print(data_ind+1)
transfer_func(FILE_NAME)
