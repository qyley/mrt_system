    %type=1,2 ASK  3,4,5 PSK 6,7,8 FSK 9-12 QAM 
    % 13 AM 14 FM 15 USB  16 LSB 
snr=10;
type=16;
while 1
    [I,Q]=signal_generate(snr,type);

    [sym,Gama_m,u42a,sigma_dp,Phase_hold] = get_feature_parm(I,Q);

    if(Phase_hold<83*pi)
        break;
    end
end
display([sym,Gama_m,u42a,sigma_dp]);

% 准备数据

data_len = 2*(8192);
udp_pkg_max_byte_len = 1472;
trans_max_len = udp_pkg_max_byte_len/4;
data_i = o2c(I,24,21);
data_q = o2c(Q,24,21);

data_tx_buf = reshape([data_q;data_i],1,[]); %I，Q交替发送

% 发送数据
board_udp = udp('192.168.1.10',1234);
board_udp.OutputBufferSize = udp_pkg_max_byte_len;
board_udp.OutputDatagramPacketSize = udp_pkg_max_byte_len;
fopen(board_udp);

trans_times = floor(data_len/trans_max_len)+1;

for i=0:trans_times-1
    i_start = i*trans_max_len+1;
    i_end   = min((i+1)*trans_max_len,data_len);
    fwrite(board_udp,data_tx_buf(i_start:i_end),'uint32');
    pause(0.001);
end

fclose(board_udp);
delete(board_udp);