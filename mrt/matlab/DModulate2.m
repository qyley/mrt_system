%--------------------------------------------------------------------
%数字调制信号仿真

%本程序仿真多种数字调制信号，包括2ASK、4ASK、MPSK、MFSK和MQAM
%输入：基带信号G、码元速率Rb、载频fc、调制方式model、进制M
%输出：已调信号Sig_Mod
%--------------------------------------------------------------------
function [Sig_Mod,hilbt]=DModulate2(G,fc,Rb,model,M,carrier_n)
%--------------------------------------------------------------------
times = carrier_n; % 每个载波周期采样times个点
fs=times*fc;
% T = nCarrPerSym;%每个码元有T个载波周期
N=fs/Rb;%N为一个码元的采样点数
len=size(G,2);     %基带信号的长度
Unit=ones(1,len);
dt = 1/fs;
t = (0:dt:(len*N-1)*dt);
%--------------------------------------------------------------------
%确定已调信号的参数：瞬时幅度a_theta，瞬时频率f_theta，瞬时相位phi_theta
%ASk调制
%--------------------------------------------------------------------
if model=='ASK'

%     xx = [];
%     for i = 1:len
%         xx = [xx, G(i)*ones(1,N)];
%     end
    
    delay=3;
    y=rcosflt(G,Rb,fs);%升余弦脉冲成型
    xx=y(fs*delay/Rb+1:end-fs*delay/Rb);
    xx = xx';
    ca = cos(2*pi*fc*t);
    ca1 = sin(2*pi*fc*t);
    if M==2
        Sig_Mod = (0.8*xx + 0.2).*ca;
    elseif M==4
        Sig_Mod = (0.25*xx + 0.25).*ca;
    end
    hilbt = xx.*ca1;
%--------------------------------------------------------------------
%%
%%--FSK
%--------------------------------------------------------------------
elseif model=='FSK'
    delta = Rb;
    ca = zeros(8,length(t));
    ca(1,:) = cos(2*pi*(fc+delta)*t);
    ca(2,:) = cos(2*pi*(fc-delta)*t);
    ca(3,:) = cos(2*pi*(fc+3*delta)*t);
    ca(4,:) = cos(2*pi*(fc-3*delta)*t);
    ca(5,:) = cos(2*pi*(fc+5*delta)*t);
    ca(6,:) = cos(2*pi*(fc-5*delta)*t);
    ca(7,:) = cos(2*pi*(fc+7*delta)*t);
    ca(8,:) = cos(2*pi*(fc-7*delta)*t);
    ca1 = zeros(8,length(t));
    ca1(1,:) = sin(2*pi*(fc+delta)*t);
    ca1(2,:) = sin(2*pi*(fc-delta)*t);
    ca1(3,:) = sin(2*pi*(fc+3*delta)*t);
    ca1(4,:) = sin(2*pi*(fc-3*delta)*t);
    ca1(5,:) = sin(2*pi*(fc+5*delta)*t);
    ca1(6,:) = sin(2*pi*(fc-5*delta)*t);
    ca1(7,:) = sin(2*pi*(fc+7*delta)*t);
    ca1(8,:) = sin(2*pi*(fc-7*delta)*t);

    arr = [G;G;G;G;G;G;G;G];
    for i = 1:M
        for j = 1:len
            if arr(i,j)~=(i-1)
                arr(i,j) = 0;
            else
                arr(i,j) = 1;
            end
        end
    end
    sig = ca;
    for i = 1:M
        xx = [];
        for j = 1:len
            xx = [xx, arr(i,j)*ones(1,N)];
        end
        sig(i,:) = xx;
    end
    Sig_Mod = zeros(1,length(t));
    hilbt = Sig_Mod;
    for i = 1:M
        Sig_Mod = Sig_Mod + sig(i,:).*ca(i,:);
        hilbt = hilbt + sig(i,:).*ca1(i,:);
    end
    delay=3;
    y=rcosflt(Unit,Rb,fs);%升余弦脉冲成型
    xx=y(fs*delay/Rb+1:end-fs*delay/Rb);
    xx = xx';
    Sig_Mod = Sig_Mod.*xx;
%         figure;
%         plot(Sig_Mod);
%--------------------------------------------------------------------
%%
%%--PSK
%--------------------------------------------------------------------
elseif model=='PSK'
    delay=3;
    y=rcosflt(Unit,Rb,fs);%升余弦脉冲成型
    xx=y(fs*delay/Rb+1:end-fs*delay/Rb);
    xx=xx';
    if M==2
        a_theta=xx;
        f_theta=fc*Unit;
        phi_theta=G*pi+pi/2;
    elseif M==4
        a_theta=1*xx;
        f_theta=fc*Unit;
        phi_theta=G*pi/2+Unit*pi/4;
    elseif M==8
        a_theta=1*xx;
        f_theta=fc*Unit;
        phi_theta=pi/8*Unit+pi/4*G;
    end;
    for j=1:len   %len 个码元
        for i=1:N  %每个码元N个采样点
            Sig_Mod(i+(j-1)*N)=a_theta(i+(j-1)*N)*...
                cos(2*pi*f_theta(j)*(i/fs)+phi_theta(j));
            hilbt(i+(j-1)*N)=a_theta(i+(j-1)*N)*...
                sin(2*pi*f_theta(j)*(i)/(fs)+phi_theta(j));
        end;
    end;

%--------------------------------------------------------------------
%QAM调制，I：同相分量，Q：正交分量
elseif model=='QAM'
    y = qammod(G,M);
    I = real(y);
    Q = imag(y);
    
%     xx = [];
%     xxx = [];
%     for i = 1:length(I)
%         xx = [xx, I(i)*ones(1,N)];
%         xxx = [xxx, Q(i)*ones(1,N)];
%     end
    delay=3;
    y=rcosflt(I,Rb,fs);%升余弦脉冲成型
    xx=y(fs*delay/Rb+1:end-fs*delay/Rb);
    
    y=rcosflt(Q,Rb,fs);%升余弦脉冲成型
    xxx=y(fs*delay/Rb+1:end-fs*delay/Rb);
    
    xx = xx';
    xxx=xxx';
    ca = cos(2*pi*fc*t);
    ca1 = sin(2*pi*fc*t);
    Sig_Mod = xx.*ca-xxx.*ca1;
    hilbt = xx.*ca1+xxx.*ca;
%     for j=1:len
%         for i=1:N
%             Sig_Mod(i+(j-1)*N)=I(j)*cos(2*pi*fc*i/fs)...
%                 +Q(j)*sin(2*pi*fc*i/fs);
% %             hilbt(i+(j-1)*N)=I(j)*sin(2*pi*fc*i/fs)...
% %                 -Q(j)*cos(2*pi*fc*i/fs);
%         end;
%     end;
end;
end