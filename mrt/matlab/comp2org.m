function data_org = comp2org(data_comp, digit, width)

if data_comp < 2^(width-1) % positive
    data_org = data_comp / 2^digit;
else % negetive
    data_org = -((2^width - data_comp) / 2^digit);
end