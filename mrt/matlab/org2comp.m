function data_comp = org2comp(data_org, digit, width)

    data_comp = mod(2.^width + data_org*2.^digit, 2.^width);