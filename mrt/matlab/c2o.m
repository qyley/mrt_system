function do = c2o(dc, w, d)

    [m, n] = size(dc);
    do = zeros(m,n);
    for i = 1:m
        for j = 1:n
            do(i,j) = comp2org(dc(i,j), d, w);
        end
    end
    
end