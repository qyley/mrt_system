


function [sym,Gama_m,u42a,sigma_dp,Phase_hold] = get_feature_parm(signal_I,signal_Q)
  N=8192;  
% 载波频率
fc=150e3; 
times=8;
% 采样率
fs=times*fc;
signal_fi_I = fi(signal_I,1,24,22);
signal_fi_Q = fi(signal_Q,1,24,22);  



Angle0=angle(signal_fi_I.data+signal_fi_Q.data*1i);

Amp0=abs(signal_fi_I.data+1i*signal_fi_Q.data);

Sig_analytic=signal_fi_I.data+1i*signal_fi_Q.data;



%%%%%%%%%%%%%%%%%%%%%%%实现数据的分帧
  measure_num =8192;
  fram_num = 3;
for i = 1:fram_num-2
    Angle1(i,:) =  Angle0((i-1)*measure_num+1:i*measure_num);
    Amp1(i,:) =  Amp0((i-1)*measure_num+1:i*measure_num);
end
%%%%%%%%%%%%%%%%%%%%%%计算P
psd = (abs(fft(Sig_analytic)));
mid = length(Sig_analytic)*0/fs;
mid = mid + length(Sig_analytic)/2;

dist = 0;

PU = sum(psd(1:mid));
PL = sum(psd(mid+1:end));
sym = (PL-PU)/(PL+PU);


%%%%%%%%%%%%%%%%%%%%%%对瞬时幅值做处理
for i = 1:fram_num-2   
    Amp1_mean(i) = sum( Amp1(i,:))/measure_num;
end
 
for i = 1:fram_num-2   
    Amp1_norm(i,:) = Amp1(i,:)/ Amp1_mean(i);
end

for i = 1:fram_num-2   
   Indices(i,:) = find(Amp1_norm(i,:)>1);
   C1(i)=length( Indices(i,:));
end


%%%%%%%%%%%%%%%计算中心归一化幅度参数Gama_m
unit=ones(1,measure_num);

for i = 1:fram_num-2   
    Amp1_norm_center(i,:) = Amp1_norm(i,:)-unit;
end

for i = 1:fram_num-2   
    fft_m(i,:) =(fft(Amp1_norm_center(i,:)));
end
% 

for i = 1:fram_num-2   
    Gama_m(i) =max(abs(fft(Amp1_norm_center(i,:))).^2)/N;
end
%  Gama_m11 =fft(Amp1_norm_center);
%  Gama_m22=max(abs(fft(Amp1_norm_center)).^2);
%  Gama_m33 =(real(fft(Amp1_norm_center)));
%  Gama_m44 =(imag(fft(Amp1_norm_center)));
%%%%%%%%%%%%%计算u42a

for i = 1:fram_num-2   
    e4(i) =sum(Amp1_norm_center(i,:).^4)/N;
    e2(i) =sum(Amp1_norm_center(i,:).^2)/N;
end

for i = 1:fram_num-2   
    u42a(i) =e4(i)/( e2(i)* e2(i));
end

%%%%%%%%%%%%%%%%%%%%%%对瞬时相位做处理

 for i = 1:fram_num-2 
     for j = 2:measure_num
     F(i,1)=0;
     F(i,j)=Angle1(i,j)-Angle1(i,j-1);
     end
 end

 for i = 1:fram_num-2 
     for j = 2:measure_num
     diff_out11(i,1)=0;
    if Angle1(i,j)-Angle1(i,j-1)<-pi
     diff_out11(i,j)=diff_out11(i,j-1)+pi*2; 
    elseif Angle1(i,j)-Angle1(i,j-1)>pi
        diff_out11(i,j)=diff_out11(i,j-1)-pi*2; 
    else
        diff_out11(i,j)=diff_out11(i,j-1);
    end
     end
 end

 for i = 1:fram_num-2   
   phi_nd(i,:) = diff_out11(i,:)+Angle1(i,:); 
 end    
  
phi_hold=phi_nd;

Phase_hold=max(abs(phi_hold));

 for i = 1:fram_num-2 
     for j = 1:measure_num
    while phi_nd(i,j)>pi
        phi_nd(i,j)=phi_nd(i,j)-2*pi;
    end;
    while phi_nd(i,j)<-pi
        phi_nd(i,j)=phi_nd(i,j)+2*pi;
    end;    
     end;
end;


for i = 1:fram_num-2   
    phi_mean(i) = sum( phi_nd(i,:))/measure_num;
end

for i = 1:fram_num-2   
    phi_norm(i,:) = phi_nd(i,:)- phi_mean(i);
end

%%%%%%%%%%%%%%%%%%%%%%计算sigmadp
for i = 1:fram_num-2   
    phi_norm_sum(i) = (sum( phi_norm(i,Indices(i,:)))/C1(i)).^2;
end


for i = 1:fram_num-2  
    phi_norm_sum2(i) = sum(  phi_norm(i,Indices(i,:)).^2 )/C1(i);
end

for i = 1:fram_num-2   
    sigma_dp(i) = sqrt(phi_norm_sum2(i) -phi_norm_sum(i));
end










