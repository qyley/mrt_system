%%
%模拟调制
%%
%st2在isb调制下需要，否则置为零即可
%fmax在vsb调制下需要，否则置为零即可
function [Sig_mod] = AModulate(st,st2,fc,fs,type,k,fmax)

% Sig_mod = modulate(st,fc,fs,type,k);
dt = 1/fs;
t = 0:dt:(length(st)-1)*dt;
if strcmpi(type,'AM')||strcmpi(type,'am')
    st = st/max(abs(st));
    Sig_mod = (1+k*st).*cos(2*pi*fc*t);
elseif strcmpi(type,'FM')||strcmpi(type,'fm')
%     Sig_mod = modulate(st,fc,fs,'fm',k);

    kf=k*2*pi*fmax;
    phi_t=kf*cumsum(st)/fs;%相位积分，要除以采样频率
    Sig_mod=cos(2*pi*fc*t+phi_t);%已调信号
elseif strcmpi(type,'USB')||strcmpi(type,'usb')
    Sig_mod = ssbmod(st,fc,fs,0,'upper');
elseif strcmpi(type,'LSB')||strcmpi(type,'lsb')
    Sig_mod = ssbmod(st,fc,fs,0);
elseif strcmpi(type,'ISB')||strcmpi(type,'isb')
    Sig_mod = ssbmod(st,fc,fs,0,'upper')+ssbmod(st2,fc,fs,0);
elseif strcmpi(type,'DSB')||strcmpi(type,'dsb')
    Sig_mod = modulate(st,fc,fs,'am',k);
elseif strcmpi(type,'VSB')||strcmpi(type,'vsb')
    s_vsb=st.*cos(2*pi*fc*t);
    B1=0.2*fmax;
    B2=1.2*fmax;
    [f,sf]=FFT_SHIFT(t,s_vsb);
    [t,Sig_mod]=vsbmd(f,sf,B1,B2,fc);
end
end