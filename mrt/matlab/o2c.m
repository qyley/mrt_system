function dc = o2c(do, w, d)

    [m, n] = size(do);
    dc = zeros(m,n);
    for i = 1:m
        for j = 1:n
            dc(i,j) = round(org2comp(do(i,j), d, w));
        end
    end
    
end