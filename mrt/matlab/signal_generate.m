
function [I,Q] = signal_generate(snr,type)
% 模拟基带信号产生滤波器
load ADQL1;
anabasecoe = ADQL1;   

% 接受基带信号滤波器
load ADQL6;
anabandcoe = ADQL6;


M=4;
% 载波频率
fc=150e3; 
times=8;
% 采样率
fs=times*fc;
numCarrierPerSymbol = 4;
numSymbols=128;
% 码元速率
Rb = 18.75e3;
dt = 1/fs;
t=dt:dt:0.1;
delta = 1;
dec = 1;
Ns=0;


%%%%%%%%%%%%%配置参数 snr 配置信噪比   i配置调制方式 
    %i=1,2 ASK  3,4,5 PSK 6,7,8 FSK 9-12 QAM 
                   % 13 AM 14 FM 15 USB  16 LSB 
   i=type;
    N=8192;
    % set parameters
    if i<3
        Modualtion_type = 'ASK';
        M = 2*i;

    elseif i<6
        Modualtion_type = 'PSK';
        M = 2^(i-2);

    elseif i<9
        Modualtion_type = 'FSK';
        M = 2^(i-5);

    elseif i<13
        Modualtion_type = 'QAM';
        M = 2^(i-6);

    elseif i==13
        Modualtion_type = 'AM';
        Ns = 8192;
        x = zeros(1,Ns);
        n = normrnd(0,1,[1,Ns]);
        tho = 0.95;
        for thisi =2:length(x)
            x(thisi) = tho*x(thisi-1)+n(thisi);
        end
      % 调制指数
        kf=0.5+randi(5,[1,1])/10;
    
    elseif i==14
        Modualtion_type = 'FM';
        Ns = 8192;
        x = zeros(1,Ns);
        n = normrnd(0,1,[1,Ns]);
        tho = 0.95;
        for thisi =2:length(x)
            x(thisi) = tho*x(thisi-1)+n(thisi);
        end
     % 调制指数
        kf=4;

    elseif i==15
        Modualtion_type = 'USB';
        
        Ns = 8192;
        x = zeros(1,Ns);
        n = normrnd(0,1,[1,Ns]);
        tho = 0.95;
        kf=0;
        for thisi =2:length(x)
            x(thisi) = tho*x(thisi-1)+n(thisi);
        end
    elseif i==16
        Modualtion_type = 'LSB';
        Ns = 8192;
        x = zeros(1,Ns);
        n = normrnd(0,1,[1,Ns]);
        tho = 0.95;
         kf=0;
        for thisi =2:length(x)
            x(thisi) = tho*x(thisi-1)+n(thisi);
        end
    end

    base = randi(M,[1,numSymbols])-1;
    figure(1);
  if(i<13)
   subplot(2,2,1);
   plot(1:numSymbols,base);
   title('数字基带信号时域波形');
   grid on;
   y_bondary = max(abs(min(base)),abs(max(base)))+1;
   axis([1 numSymbols -1 y_bondary]);
   xlabel('time');
   ylabel('Amplitude')
  else
   subplot(2,2,1);
   plot(1:Ns,x);
   title('模拟基带信号时域波形');
   grid on;
   y_bondary = max(abs(min(x)),abs(max(x)))+1;
   axis([1 Ns -y_bondary y_bondary]);
   xlabel('time');
   ylabel('Amplitude')
  end
   
   if i<13
        [re,im] = DModulate2(base,fc,Rb,Modualtion_type,M,times);
    else
        x = filter(anabasecoe,1,x);
        re = AModulate(x,0,fc,fs,Modualtion_type,kf,5000);

    end

    re = awgn(re,snr,'measured');
    re = re/max(abs(re));
%     [IMT,WT,HOC,symc] = getCharacters(re,Rb,fc,times,anabandcoe,dec,i);

    % 画图
   subplot(2,2,2);
   plot(1:N,re);
   title([Modualtion_type,'调制信号时域波形']);
   grid on;
   xlabel('time');
   ylabel('Amplitude') 
   axis([0 8192 -2 2]);
    
   subplot(2,2,3);
   plot((-0.5:1/(N):0.5-1/(N))*fs,20*log10(fftshift(abs(fft(re)))));
   title([Modualtion_type,'调制信号频谱']);
   grid on;
   xlabel('Frequency (Hz)');
   ylabel('Amplitude (dB)');


 %%%%数字下变频     
 carrier = exp(-1i*(2*pi*fc*(0:1:length(re)-1)/fs));
 ddcsig = re.*carrier;
 b = anabandcoe;
 ddcsig = filter(b,1,ddcsig);
 
 
 subplot(2,2,4);
 plot((-0.5:1/(N):0.5-1/(N))*fs,20*log10(fftshift(abs(fft(ddcsig)))));
 title('接收基带调制信号频谱');
 grid on;
 xlabel('Frequency (Hz)');
 ylabel('Amplitude (dB)');
  
 
I=real(ddcsig);
Q=imag(ddcsig);
 
 
% signal_fi_I = fi(signal_I,1,24,22);
% signal_fi_Q = fi(signal_Q,1,24,22);
 


